﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc.Filters;
using MI.Framework.Persistence;

namespace MI.UserManager.Web.FIlters
{
    public class PersistenceScopeFilter : ActionFilterAttribute
    {
        private readonly PersistenceScope _persistenceScope;

        public PersistenceScopeFilter(PersistenceScope persistenceScope)
        {
            _persistenceScope = persistenceScope;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Debug.WriteLine("OnActionExecuting");
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if(!context.ExceptionHandled)
                _persistenceScope.Done();
            Debug.WriteLine("OnActionExecuted");
        }
    }
}
