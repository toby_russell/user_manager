﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class PermissionAssignmentParameterController : Controller
    {

        public PermissionAssignmentParameterController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<PermissionAssignmentParameter>().Select(pap => new { pap.InternalId, pap.Name, pap.Description, pap.Value }).ToList();
                scope.Done();

                return Ok(ret);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<PermissionAssignmentParameter>().Select(pap => new { pap.InternalId, pap.Name, pap.Description, pap.Value }).FirstOrDefault(t => t.InternalId == internalId);
                scope.Done();

                if (ret == null)
                    return NotFound();
                return new ObjectResult(ret);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] PermissionAssignmentParameter permissionAssignmentParameter)
        {
            if (permissionAssignmentParameter == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                scope.Persist(permissionAssignmentParameter);
                scope.Done();
            }

            return new NoContentResult();
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] PermissionAssignmentParameter permissionAssignmentParameter)
        {
            if (permissionAssignmentParameter == null || permissionAssignmentParameter.InternalId != internalId)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var existingTemplate = scope.Query<PermissionAssignmentParameter>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null)
                    return NotFound();

                existingTemplate = permissionAssignmentParameter;
                scope.Update(existingTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }

        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ptps = scope.Query<PermissionAssignmentParameter>().ToList().Where(r => r.InternalId == internalId);

                foreach (var ptp in ptps)
                    scope.Delete(ptp);

                scope.Done();

                return new NoContentResult();
            }
        }

    }
}
