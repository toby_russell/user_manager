﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Models;

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class PermissionTemplateParameterController : Controller
    {

        public PermissionTemplateParameterController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<PermissionTemplateParameter>().Select(ptp => new { ptp.InternalId, ptp.Name, ptp.Description, ptp.DefaultValue }).ToList();

                scope.Done();
                return Ok(ret);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<PermissionTemplateParameter>().Select(ptp => new { ptp.InternalId, ptp.Name, ptp.Description, ptp.DefaultValue }).FirstOrDefault(t => t.InternalId == internalId);
                scope.Done();

                if (ret == null)
                    return NotFound();
                return new OkObjectResult(ret);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] PermissionTemplateParameter permissionTemplateParameter)
        {
            if (permissionTemplateParameter == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                scope.Persist(permissionTemplateParameter);
                scope.Done();
            }

            return new NoContentResult();
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] PermissionTemplateParameter permissionTemplateParameter)
        {
            if (permissionTemplateParameter == null || permissionTemplateParameter.InternalId != internalId)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var existingTemplate = scope.Query<PermissionTemplateParameter>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null)
                    return NotFound();

                existingTemplate = permissionTemplateParameter;
                scope.Update(existingTemplate);

                scope.Done();

                return new OkResult();
            }
        }

        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ptps = scope.Query<PermissionTemplateParameter>().ToList().Where(r => r.InternalId == internalId);

                foreach (var ptp in ptps)
                    scope.Delete(ptp);

                scope.Done();

                return new OkResult();
            }
        }

    }
}
