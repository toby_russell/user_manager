﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Models;
using NHibernate.Linq;
using NHibernate.Mapping;
using NHibernate.Util;

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class RoleTemplateController : Controller
    {
        public RoleTemplateController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var roletemps = scope.Query<RoleTemplate>().ToList();

                scope.Done();

                var ret = new List<RoleViewModel>();

                if (roletemps != null)
                {
                    foreach (var rt in roletemps)
                    {
                        ret.Add(RoleFactory.CreateNewRoleViewModel(rt));
                    }
                }
                
                return new OkObjectResult(ret);
            }
        }

        [HttpGet(),
         Route("[action]/{userInternalId}")]
        public IActionResult GetFilteredByUserInternalId(Guid userInternalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var rolePermIds = scope.Query<ApplicationUser>().FirstOrDefault(u => u.InternalId == userInternalId)
                    ?.Roles.Select(r => r.RoleTemplate?.InternalId);

                if (rolePermIds != null)
                {
                    var baseQuery = scope.Query<RoleTemplate>()
                        .Where(ra => !rolePermIds.Contains(ra.InternalId));

                    baseQuery.FetchMany(p => p.Parameters).ThenFetch(p => p.Type).ToFuture();

                    var res = baseQuery.ToFuture().ToList();

                    scope.Done();

                    var ret = new List<RoleViewModel>();

                    foreach (var rt in res)
                    {
                        ret.Add(RoleFactory.CreateNewRoleViewModel(rt));
                    }

                    return new OkObjectResult(ret);
                }
            }

            return NotFound();
        }

        [HttpGet,
         Route("[action]")]
        public IActionResult GetRoleTemplateParameters()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                return new OkObjectResult(scope.ListAll<ParameterType>().ToList());
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                //does not produce cartesian; uses 1 db call
                var baseQuery = scope.Query<RoleTemplate>()
                    .Where(rt => rt.InternalId == internalId);

                baseQuery.FetchMany(r => r.PermissionTemplates).ToFuture();
                baseQuery.FetchMany(r => r.Parameters).ThenFetch(p => p.Type).ToFuture();

                var expectedResult = baseQuery.ToFuture();
                
                scope.Done();

                RoleTemplate result = expectedResult.FirstOrDefault();
                if (result == null)
                    return NotFound();

                return new OkObjectResult(RoleFactory.CreateNewRoleViewModel(result));
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] RoleViewModel roleTemplateVm)
        {
            if (roleTemplateVm == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var roleTemplate = new RoleTemplate(roleTemplateVm.name, roleTemplateVm.description, Guid.NewGuid());
                var paramsSet = new HashSet<RoleTemplateParameter>();

                var permParams = new HashSet<ParameterViewModel>();
                var roleParams = new HashSet<ParameterViewModel>();

                var permsSet = new HashSet<PermissionTemplate>();
                
                foreach (var permVm in roleTemplateVm.permissions)
                {
                    var permTemp = new PermissionTemplate(permVm.name, permVm.claimType, permVm.description, permVm.internalId);
                    var permParamsSet = new HashSet<PermissionTemplateParameter>();

                    foreach(var ptp in permVm.parameters)
                    {
                        permParamsSet.Add(new PermissionTemplateParameter(ptp.name, ptp.description, ptp.internalId,
                            AssociateAndPersistParameterType(scope, ptp.typeInternalId, ptp.typeName, ptp.name, ptp.description),
                            ptp.value));
                        permParamsSet.FirstOrDefault(p => p.InternalId == ptp.internalId).ParentPermissionTemplate = permTemp;

                        var rtp = new RoleTemplateParameter(ptp.name, ptp.description, Guid.NewGuid(),
                            AssociateAndPersistParameterType(scope, ptp.typeInternalId, ptp.typeName, ptp.name, ptp.description),
                            ptp.value);
                        rtp.ParentRoleTemplate = roleTemplate;
                        roleTemplate.Parameters.Add(rtp);
                    }

                    permTemp.Parameters = permParamsSet;
                    permsSet.Add(permTemp);
                }
                
                roleTemplate.PermissionTemplates = permsSet;

                scope.Persist(roleTemplate);
                scope.Done();
            }

            return new NoContentResult();
        }

        private ParameterType AssociateAndPersistParameterType(PersistenceScope scope, Guid internalId, string typeName = null, string paramTypeName = null, string paramTypeDescription = null)
        {
            ParameterType type = null;
            if (internalId != null && internalId != Guid.Empty && internalId != default(Guid)) //already exists
            {
                type = scope.Query<ParameterType>().FirstOrDefault(pt => pt.InternalId == internalId);

                if (type != null && type != default(ParameterType))
                {
                    return type;
                }
            }
            else
            {
                switch (typeName)
                {
                    case "BoolParam":
                        type = new BoolParameterType(paramTypeName, paramTypeDescription, Guid.NewGuid());
                        break;
                    case "SiteParam":
                        type = new SiteParameterType(paramTypeName, paramTypeDescription, Guid.NewGuid());
                        break;
                    case "StringParam":
                    default:
                        type = new StringParameterType(paramTypeName, paramTypeDescription, Guid.NewGuid());
                        break;
                }
            }

            scope.Persist(type);
            
            return type;
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] RoleViewModel roleTemplate)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                if (roleTemplate == null)
                    return NotFound();

                var existingTemplate = scope.Query<RoleTemplate>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null)
                    return NotFound();
                else
                {
                    existingTemplate.Name = roleTemplate.name;
                    existingTemplate.Description = roleTemplate.description;
                    
                    var permList = new HashSet<PermissionTemplate>();
                    var roleParamList = new HashSet<RoleTemplateParameter>();

                    //add permissions
                    foreach (var permission in roleTemplate.permissions)
                    {
                        var permGuid = permission.internalId == Guid.Empty ? Guid.NewGuid() : permission.internalId;
                        var existingPerm = new PermissionTemplate(permission.name, permission.claimType, permission.description, permGuid);
                        var permParamList = new HashSet<PermissionTemplateParameter>();
                            
                        foreach(var permParameter in permission.parameters)
                        {
                            var permParamGuid = permParameter.internalId == Guid.Empty ? Guid.NewGuid() : permParameter.internalId;
                            var existingParameter = new PermissionTemplateParameter(permParameter.name, permParameter.description, permParamGuid,
                                AssociateAndPersistParameterType(scope, permParameter.typeInternalId),
                                permParameter.value);
                            existingParameter.ParentPermissionTemplate = existingPerm;

                            permParamList.Add(existingParameter);

                            //see if there's a matching element in the role parameters
                            //if so, add it using it's guid, otherwise make a new one
                            var temptemplate = existingTemplate.Parameters.FirstOrDefault(p => p.Name == permParameter.name && p.Description == permParameter.description && p.DefaultValue == permParameter.value);
                            var rtpGuid = temptemplate != null ? temptemplate.InternalId : Guid.NewGuid();

                            var rtp = new RoleTemplateParameter(permParameter.name, permParameter.description, rtpGuid,
                                AssociateAndPersistParameterType(scope, permParameter.typeInternalId),
                                permParameter.value);
                            rtp.ParentRoleTemplate = existingTemplate;
                            roleParamList.Add(rtp);
                        }

                        existingPerm.Parameters = permParamList;
                        permList.Add(existingPerm);
                    }

                    existingTemplate.PermissionTemplates = permList;
                    existingTemplate.Parameters = roleParamList;
                }
                scope.Update(existingTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }

        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var roleTemplate = scope.Query<RoleTemplate>().FirstOrDefault(r => r.InternalId == internalId);

                var roleAssignments = scope.Query<RoleAssignment>().Where(ra => ra.RoleTemplate.InternalId == roleTemplate.InternalId).ToList();

                var roleTemplateParams = scope.Query<RoleTemplateParameter>().Where(rtp => rtp.InternalId == internalId).ToList();

                foreach(var ra in roleAssignments)
                {
                    ra.RoleTemplate = null;
                }

                foreach(var rtp in roleTemplateParams)
                {
                    scope.Delete(rtp);
                }

                scope.Delete(roleTemplate);
                
                scope.Done();

                return new NoContentResult();
            }
        }
    }


}
