﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MI.Framework.Persistence;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Models;
using MI.UserManager.Web.Shared.Services;
using Newtonsoft.Json;
using NHibernate.Util;
using NHibernate.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class ApplicationUserController : Controller
    {
        private readonly IHashPasswords _hashPasswords;
        private IUserService _userService;

        public ApplicationUserController(IHashPasswords hashPasswords, IUserService userService)
        {
            _hashPasswords = hashPasswords;
            _userService = userService;
        }

        // GET: api/<controller>
        [HttpGet, 
         Route("")]
        //public IEnumerable<ApplicationUser> Get()
        public IActionResult Get()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                //                var users = scope.ListAll<ApplicationUser>().ToList();
                var baseQuery = scope.Query<ApplicationUser>();

                baseQuery.Fetch(u => u.Organisation).ToFuture();
                baseQuery.FetchMany(u => u.Roles).ThenFetchMany(p => p.Parameters).ThenFetch(t => t.Type).ToFuture();
                baseQuery.FetchMany(u => u.Roles).ThenFetch(p => p.RoleTemplate).ThenFetchMany(t => t.Parameters).ThenFetch(p => p.Type).ToFuture();
                baseQuery.FetchMany(u => u.Permissions).ThenFetchMany(p => p.Parameters).ThenFetch(t => t.Type).ToFuture();
                baseQuery.FetchMany(u => u.Permissions).ThenFetch(p => p.PermissionTemplate).ThenFetchMany(t => t.Parameters).ThenFetch(p => p.Type).ToFuture();

                var queryResult = baseQuery.ToFuture();
                scope.Done();

                var users = queryResult.ToList();
                var ret = new List<ApplicationUserViewModel>();

                if (ret == null)
                    return NotFound($"No users found.");
                
                foreach(var u in users)
                {
                    ret.Add(ApplicationUserFactory.CreateNewApplicationUserViewModel(u));
                }
                
                return new OkObjectResult(ret);
            }
        }

        // GET api/<controller>/5
        [HttpGet("{internalId}"),
         Authorize]
        public IActionResult Get(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var baseQuery = scope.Query<ApplicationUser>().Where(u => u.InternalId == internalId);

                baseQuery.Fetch(u => u.Organisation).ToFuture();
                baseQuery.FetchMany(u => u.Roles).ThenFetchMany(p => p.Parameters).ThenFetch(t => t.Type).ToFuture();
                baseQuery.FetchMany(u => u.Roles).ThenFetch(p => p.RoleTemplate).ThenFetchMany(t => t.Parameters).ThenFetch(p => p.Type).ToFuture();
                baseQuery.FetchMany(u => u.Permissions).ThenFetchMany(p => p.Parameters).ThenFetch(t => t.Type).ToFuture();
                baseQuery.FetchMany(u => u.Permissions).ThenFetch(p => p.PermissionTemplate).ThenFetchMany(t => t.Parameters).ThenFetch(p => p.Type).ToFuture();

                var queryResult = baseQuery.ToFuture();

                scope.Done();

                var user = queryResult.FirstOrDefault();

                if (user == null)
                    return NotFound($"User with ID of { internalId } was not found.");

                return new OkObjectResult(ApplicationUserFactory.CreateNewApplicationUserViewModel(user));
            }
        }


        //TODO
        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]ApplicationUserViewModel user)
        {
            var newId = Guid.Empty;

            if (user == null)
                return BadRequest(); //new NotFoundObjectResult(newId);

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                //check that a user with the given ID doesn't already exist
                if (scope.Query<ApplicationUser>().FirstOrDefault(u => u.InternalId == user.internalId) == null)
                {
                    user.internalId = Guid.NewGuid();
                    var newUser = new ApplicationUser(user.firstName, user.lastName, user.isEnabled, user.email, user.internalId);

                    newUser.Organisation = scope.Query<Organisation>().FirstOrDefault(o => o.Name == "Manufacturing Intelligence");
                    string pw = "password123";
                    var salt = _hashPasswords.CreateSalt(pw.Length);
                    var hashedPw = _hashPasswords.HashPassword(Encoding.UTF8.GetBytes(pw), salt);
                    newUser.PasswordHash = hashedPw;
                    newUser.PasswordSalt = Convert.ToBase64String(salt);
                    
                    foreach (var roleAssignment in user.roles)
                    {
                        var newRole = new RoleAssignment(roleAssignment.name, roleAssignment.description, Guid.NewGuid(),
                            scope.Query<RoleTemplate>().FirstOrDefault(rt => rt.InternalId == roleAssignment.internalId));
                        
                        var roleParamList = new HashSet<RoleAssignmentParameter>();

                        //add parameters - these should match 1:1 with the permission assignment parameters just below this loop
                        foreach (var roleParameter in roleAssignment.parameters)
                        {
                                roleParamList.Add(new RoleAssignmentParameter(roleParameter.name, roleParameter.description, Guid.NewGuid(),
                                    AssociateAndPersistParameterType(scope, roleParameter.typeInternalId),
                                    roleParameter.value));
                        }

                        newRole.Parameters = roleParamList;

                        var permList = new HashSet<PermissionAssignment>();

                        //add permissions
                        foreach (var permission in roleAssignment.permissions)
                        {
                            var newPerm = new PermissionAssignment(permission.name, permission.claimType, permission.description, Guid.NewGuid(), 
                                scope.Query<PermissionTemplate>().FirstOrDefault(pt => pt.InternalId == permission.internalId));

                            var permParamList = new HashSet<PermissionAssignmentParameter>();

                            foreach (var permParameter in permission.parameters)
                            {
                                permParamList.Add(new PermissionAssignmentParameter(permParameter.name, permParameter.description, permParameter.internalId,
                                    AssociateAndPersistParameterType(scope, permParameter.typeInternalId),
                                    permParameter.value));                                
                            }

                            newPerm.Parameters = permParamList;
                            permList.Add(newPerm);
                        }

                        newRole.PermissionAssignments = permList;
                        newUser.Roles.Add(newRole);
                    }

                    //permissions assigned only to this user, not to a role
                    foreach (var permAssignment in user.permissions)
                    {
                        PermissionAssignment newPerm = new PermissionAssignment(permAssignment.name, permAssignment.claimType, permAssignment.description, Guid.NewGuid(), 
                            scope.Query<PermissionTemplate>().FirstOrDefault(pt => pt.InternalId == permAssignment.internalId));

                        var permParamList = new HashSet<PermissionAssignmentParameter>();

                        foreach (var param in permAssignment.parameters)
                        {
                            var newParam = new PermissionAssignmentParameter(param.name, param.description, Guid.NewGuid(),
                                AssociateAndPersistParameterType(scope, param.typeInternalId),
                                param.value);

                            permParamList.Add(newParam);
                        }

                        newPerm.Parameters = permParamList;

                        newUser.Permissions.Add(newPerm);    
                    }

                    scope.Persist(newUser);

                    _userService.SendConfirmEmailAddressAndChangePasswordEmail(newUser.Email);
                    newId = newUser.InternalId;
                    scope.Done();
                    
                }
                return new NoContentResult(); //OkObjectResult(newId);

            }
        }

        private ParameterType AssociateAndPersistParameterType(PersistenceScope scope, Guid internalId, string typeName = null, string paramTypeName = null, string paramTypeDescription = null)
        {
            ParameterType type = null;
            if (internalId != null && internalId != Guid.Empty && internalId != default(Guid)) //already exists
            {
                type = scope.Query<ParameterType>().FirstOrDefault(pt => pt.InternalId == internalId);

                if (type != null && type != default(ParameterType))
                {
                    return type;
                }
            }
            else
            {
                switch (typeName)
                {
                    case "BoolParam":
                        type = new BoolParameterType(paramTypeName, paramTypeDescription, Guid.NewGuid());
                        break;
                    case "SiteParam":
                        type = new SiteParameterType(paramTypeName, paramTypeDescription, Guid.NewGuid());
                        break;
                    case "StringParam":
                    default:
                        type = new StringParameterType(paramTypeName, paramTypeDescription, Guid.NewGuid());
                        break;
                }
            }

            scope.Persist(type);

            return type;
        }

        //TODO
        // PUT api/<controller>/5
        [HttpPut("{internalId}")]
        public IActionResult Put(Guid internalId, [FromBody]ApplicationUserViewModel user)
        {
            if (user != null)
            {
                using (var scope = new PersistenceScope(TransactionOption.Required))
                {
                    var query = scope.Query<ApplicationUser>().FirstOrDefault(au => au.InternalId == internalId);
                    var org = scope.Query<Organisation>().FirstOrDefault(o => o.Name == "Manufacturing Intelligence");
                    var existingUser = query;

                    if (existingUser != null)
                    {
                        existingUser.Email = user.email;
                        existingUser.FirstName = user.firstName;
                        existingUser.InternalId = user.internalId;

                        existingUser.IsEnabled = user.isEnabled;
                        existingUser.LastName = user.lastName;
                        existingUser.Organisation = org; //user.organisationId);

                        //roles...
                        var roleList = new HashSet<RoleAssignment>();
                        foreach (var roleAssignment in user.roles)
                        {
                            var roleQuery = existingUser.Roles.FirstOrDefault(ra => ra.InternalId == roleAssignment.internalId);
                            var existingRole = roleQuery;

                            if (existingRole == null) //if the role doesn't already exist, it's coming in as a role template
                            {
                                existingRole = new RoleAssignment();
                                existingRole.RoleTemplate = scope.Query<RoleTemplate>().FirstOrDefault(rt => rt.InternalId == roleAssignment.internalId);
                                existingRole.InternalId = Guid.NewGuid();
                            }

                            existingRole.Name = roleAssignment.name;
                            existingRole.Description = roleAssignment.description;

                            var roleParamList = new HashSet<RoleAssignmentParameter>();

                            //add parameters
                            foreach (var roleParameter in roleAssignment.parameters)
                            {
                                var existingParameter = existingRole.Parameters.Where(p => p.InternalId == roleParameter.internalId).FirstOrDefault();

                                if (existingParameter == null)
                                {
                                    existingParameter = new RoleAssignmentParameter();
                                    existingParameter.InternalId = Guid.NewGuid();
                                }

                                existingParameter.Name = roleParameter.name;
                                existingParameter.Type = AssociateAndPersistParameterType(scope, roleParameter.typeInternalId);
                                existingParameter.Value = roleParameter.value;
                                existingParameter.Description = roleParameter.description;

                                roleParamList.Add(existingParameter);
                            }

                            existingRole.Parameters.Clear();
                            roleParamList.ForEach(rp => existingRole.Parameters.Add(rp));

                            var rolepermList = new HashSet<PermissionAssignment>();

                            //add role permissions
                            foreach (var permission in roleAssignment.permissions)
                            {
                                var existingPerm = existingRole.PermissionAssignments.Where(pt => pt.InternalId == permission.internalId).FirstOrDefault();

                                if (existingPerm == null)
                                {
                                    existingPerm = new PermissionAssignment();
                                    existingPerm.PermissionTemplate = scope.Query<PermissionTemplate>().FirstOrDefault(pt => pt.InternalId == permission.internalId);
                                    existingPerm.InternalId = Guid.NewGuid();
                                }

                                existingPerm.ClaimType = permission.claimType;
                                existingPerm.Description = permission.description;
                                existingPerm.Name = permission.name;

                                var permParamList = new HashSet<PermissionAssignmentParameter>();

                                foreach (var permParameter in permission.parameters)
                                {
                                    var existingPermParameter = existingPerm.Parameters.Where(ppt => ppt.InternalId == permParameter.internalId).FirstOrDefault();

                                    if (existingPermParameter == null)
                                    {
                                        existingPermParameter = new PermissionAssignmentParameter();
                                        existingPermParameter.InternalId = Guid.NewGuid();
                                    }

                                    existingPermParameter.Name = permParameter.name;
                                    existingPermParameter.Type = AssociateAndPersistParameterType(scope, permParameter.typeInternalId);
                                    existingPermParameter.Value = permParameter.value;
                                    existingPermParameter.Description = permParameter.description;

                                    permParamList.Add(existingPermParameter);
                                }

                                existingPerm.Parameters.Clear();
                                permParamList.ForEach(pp => existingPerm.Parameters.Add(pp));
                                rolepermList.Add(existingPerm);
                            }

                            //existingRole.PermissionAssignments = rolepermList;
                            existingRole.PermissionAssignments.Clear();
                            rolepermList.ForEach(rp => existingRole.PermissionAssignments.Add(rp));
                            roleList.Add(existingRole);
                        }

                        existingUser.Roles.Clear();
                        roleList.ForEach(r => existingUser.Roles.Add(r));

                        //permissions that are assigned directly to this user and are not part of a role...
                        var permList = new HashSet<PermissionAssignment>();
                        foreach (var permAssignment in user.permissions)
                        {
                            var permQuery = existingUser.Permissions.FirstOrDefault(pa => pa.InternalId == permAssignment.internalId);
                            var newPerm = permQuery;
                            //PermissionAssignment newPerm = scope.Query<PermissionAssignment>().FirstOrDefault(pa => pa.InternalId == permAssignment.internalId);

                            if (newPerm == null)
                            {
                                newPerm = new PermissionAssignment();

                                newPerm.InternalId = Guid.NewGuid();
                                newPerm.PermissionTemplate = scope.Query<PermissionTemplate>().FirstOrDefault(pt => pt.InternalId == permAssignment.internalId);
                            }

                            newPerm.ClaimType = permAssignment.claimType;
                            newPerm.Description = permAssignment.description;
                            newPerm.Name = permAssignment.name;

                            var permParamList = new HashSet<PermissionAssignmentParameter>();

                            foreach (var param in permAssignment.parameters)
                            {
                                var existingParam = newPerm.Parameters.FirstOrDefault(pm => pm.InternalId == param.internalId);

                                if (existingParam == null)
                                {
                                    existingParam = new PermissionAssignmentParameter();
                                    existingParam.InternalId = Guid.NewGuid();
                                }

                                existingParam.Description = param.description;
                                existingParam.Name = param.name;
                                existingParam.Type = AssociateAndPersistParameterType(scope, param.typeInternalId);
                                existingParam.Value = param.value;

                                permParamList.Add(existingParam);
                            }

                            newPerm.Parameters.Clear();
                            permParamList.ForEach(pp => newPerm.Parameters.Add(pp));
                            permList.Add(newPerm);
                        }

                        existingUser.Permissions.Clear();
                        permList.ForEach(p => existingUser.Permissions.Add(p));

                        scope.Update(existingUser);

                        scope.Done();
                        return new OkObjectResult(true);
                    }
                }
            }

            return new NotFoundResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{internalId}")]
        public void Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var user = scope.Query<ApplicationUser>().FirstOrDefault(au => au.InternalId == internalId);

                scope.Delete(user);
                scope.Done();
            }
        }

        [HttpPut("[action]/{internalId}")]
        public IActionResult ResetPassword(Guid internalId, [FromBody]ResetPasswordViewModel user)
        {
            if (user.newPassword.Equals(user.confirmNewPassword))
            {
                using (var scope = new PersistenceScope(TransactionOption.Required))
                {
                    var oldUser = scope.Query<ApplicationUser>().FirstOrDefault(au =>
                        au.InternalId == internalId);

                    if (oldUser != null)
                    {
                        var password = user.oldPassword;
                        var decodedSalt = Convert.FromBase64String(oldUser.PasswordSalt);
                        var hashed = _hashPasswords.HashPassword(Encoding.UTF8.GetBytes(password), decodedSalt);
                        var storedHash = oldUser.PasswordHash;
                        if (!storedHash.Equals(hashed))
                            return new BadRequestResult();
                        
                        var newPassword = user.newPassword;
                        var newSalt = _hashPasswords.CreateSalt(newPassword.Length);
                        var newHashedPassword =
                            _hashPasswords.HashPassword(Encoding.UTF8.GetBytes(newPassword), newSalt);

                        oldUser.PasswordHash = newHashedPassword;
                        oldUser.PasswordSalt = Convert.ToBase64String(newSalt);
                        // oldUser.IsEnabled = user.isEnabled;

                        scope.Persist(oldUser);
                        scope.Done();
                        return new OkResult();
                    }
                    return new BadRequestResult();

                    
                }
            }
            return new BadRequestResult();
        }
    }


}
