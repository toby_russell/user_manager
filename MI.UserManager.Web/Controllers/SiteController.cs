﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MI.Framework.Persistence;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Models;

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class SiteController : Controller
    {
        public SiteController()
        {
        }

        [HttpGet]
        public IActionResult Get()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<Site>().Select(o => new SiteViewModel { InternalId = o.InternalId, Name = o.Name, Description = o.Description }).ToList();

                scope.Done();
                return new OkObjectResult(ret);
            }
        }

        //[HttpGet("[action]/{orgInternalId}")]
        //public IActionResult GetByOrganisationId(Guid orgInternalId)
        //{
        //    using (var scope = new PersistenceScope(TransactionOption.Required))
        //    {
        //        var ret = scope.Query<Site>().Select(o => new SiteViewModel { InternalId = o.InternalId, Name = o.Name, Description = o.Description }).Where(s => s.OrganisationId == orgInternalId).ToList();

        //        scope.Done();
        //        return Ok(ret);
        //    }
        //}

        [HttpGet("{internalId}")]
        public IActionResult Get(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var item = scope.Query<Site>().Select(o => new SiteViewModel { InternalId = o.InternalId, Name = o.Name, Description = o.Description }).FirstOrDefault(o => o.InternalId == internalId);
                scope.Done();

                if (item == null)
                    return NotFound();

                return new OkObjectResult(item);
            }
        }
    }
}