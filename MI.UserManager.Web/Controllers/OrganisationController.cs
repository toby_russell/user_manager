﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class OrganisationController : Controller
    {
        public OrganisationController()
        {
        }

        [HttpGet]
        public IActionResult Get()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<Organisation>().Select(o => new OrganisationViewModel { InternalId = o.InternalId, Name = o.Name, Domain = o.Domain }).ToList();

                scope.Done();
                return Ok(ret);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult Get(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var item = scope.Query<Organisation>().Select(o => new OrganisationViewModel { InternalId = o.InternalId, Name = o.Name, Domain = o.Domain }).FirstOrDefault(o => o.InternalId == internalId);
                scope.Done();

                if (item == null)
                    return NotFound();

                return new ObjectResult(item);
            }
        }
    }
}
