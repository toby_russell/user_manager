﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Models;

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class RoleTemplateParameterController : Controller
    {
        
        public RoleTemplateParameterController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<RoleTemplateParameter>().Select(rtp => new ParameterViewModel(rtp.InternalId, rtp.Name, rtp.Description, rtp.Type.Name, rtp.DefaultValue, rtp.Type.InternalId)).ToList(); // { rtp.InternalId, rtp.Name, rtp.Description, rtp.DefaultValue }).ToList();

                scope.Done();

                return Ok(ret);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<RoleTemplateParameter>().Select(rtp => new ParameterViewModel(rtp.InternalId, rtp.Name, rtp.Description, rtp.Type.Name, rtp.DefaultValue, rtp.Type.InternalId)).FirstOrDefault(t => t.internalId == internalId);
                scope.Done();

                if (ret == null)
                    return NotFound();
                return new ObjectResult(ret);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] RoleTemplateParameter roleTemplateParameter)
        {
            if (roleTemplateParameter == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                scope.Persist(roleTemplateParameter);
                scope.Done();
            }

            return new NoContentResult();
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] RoleTemplateParameter roleTemplateParameter)
        {
            if (roleTemplateParameter == null || roleTemplateParameter.InternalId != internalId)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var existingTemplate = scope.Query<RoleTemplateParameter>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null)
                    return NotFound();

                existingTemplate = roleTemplateParameter;
                scope.Update(existingTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }

        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var rtps = scope.Query<RoleTemplateParameter>().ToList().Where(r => r.InternalId == internalId);

                foreach (var rtp in rtps)
                    scope.Delete(rtp);

                scope.Done();

                return new NoContentResult();
            }
        }
        
    }
}
