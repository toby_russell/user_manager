﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class RoleAssignmentParameterController : Controller
    {

        public RoleAssignmentParameterController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<RoleAssignmentParameter>().Select(rap => new { rap.InternalId, rap.Name, rap.Description, rap.Value }).ToList();

                scope.Done();
                return Ok(ret);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<RoleAssignmentParameter>().Select(rap => new { rap.InternalId, rap.Name, rap.Description, rap.Value }).FirstOrDefault(t => t.InternalId == internalId);
                scope.Done();

                if (ret == null)
                    return NotFound();
                return new ObjectResult(ret);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] RoleAssignmentParameter roleAssignmentParameter)
        {
            if (roleAssignmentParameter == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                scope.Persist(roleAssignmentParameter);
                scope.Done();
            }

            return new NoContentResult();
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] RoleAssignmentParameter roleAssignmentParameter)
        {
            if (roleAssignmentParameter == null || roleAssignmentParameter.InternalId != internalId)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var existingTemplate = scope.Query<RoleAssignmentParameter>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null)
                    return NotFound();

                existingTemplate = roleAssignmentParameter;
                scope.Update(existingTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }

        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var rtps = scope.Query<RoleAssignmentParameter>().ToList().Where(r => r.InternalId == internalId);

                foreach (var rtp in rtps)
                    scope.Delete(rtp);

                scope.Done();

                return new NoContentResult();
            }
        }

    }
}
