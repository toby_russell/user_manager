﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;


namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class RoleAssignmentController : Controller
    {
        public RoleAssignmentController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<RoleAssignment>().Select(rt => new { rt.InternalId, rt.Name, rt.Description }).ToList();

                scope.Done();
                return Ok(ret);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<RoleAssignment>().Select(rt => new { rt.InternalId, rt.Name, rt.Description }).FirstOrDefault(t => t.InternalId == internalId);
                scope.Done();

                if (ret == null)
                    return NotFound();
                return new ObjectResult(ret);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] RoleAssignment roleAssignment)
        {
            if (roleAssignment == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                scope.Persist(roleAssignment);
                scope.Done();
            }

            return new NoContentResult();
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] RoleAssignment roleAssignment)
        {
            if (roleAssignment == null || roleAssignment.InternalId != internalId)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var existingTemplate = scope.Query<RoleAssignment>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null)
                    return NotFound();

                existingTemplate = roleAssignment;
                scope.Update(existingTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }


        //TODO
        //more complex than it seems
        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var roleAssignment = scope.Query<RoleAssignment>().FirstOrDefault(r => r.InternalId == internalId);

                scope.Delete(roleAssignment);

                scope.Done();

                return new NoContentResult();
            }
        }
    }
}
