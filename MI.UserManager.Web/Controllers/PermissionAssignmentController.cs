﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Model;


namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class PermissionAssignmentController : Controller
    {
        public PermissionAssignmentController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<PermissionAssignment>().Select(pa => new { pa.InternalId, pa.Name, pa.ClaimType, pa.Description }).ToList();

                scope.Done();
                return Ok(ret);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var ret = scope.Query<PermissionAssignment>().Select(pa => new { pa.InternalId, pa.Name, pa.ClaimType, pa.Description }).FirstOrDefault(t => t.InternalId == internalId);
                scope.Done();

                if (ret == null)
                    return NotFound();
                return new ObjectResult(ret);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] PermissionAssignment permissionAssignment)
        {
            if (permissionAssignment == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                scope.Persist(permissionAssignment);
                scope.Done();
            }

            return new NoContentResult();
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] PermissionAssignment permissionAssignment)
        {
            if (permissionAssignment == null || permissionAssignment.InternalId != internalId)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var existingTemplate = scope.Query<PermissionAssignment>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null)
                    return NotFound();

                existingTemplate = permissionAssignment;
                scope.Update(existingTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }

        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var permissionAssignment = scope.Query<PermissionAssignment>().FirstOrDefault(r => r.InternalId == internalId);

                scope.Delete(permissionAssignment);

                scope.Done();

                return new NoContentResult();
            }
        }
    }
}
