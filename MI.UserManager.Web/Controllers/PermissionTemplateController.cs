﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Razor;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Models;
using NHibernate.Linq;

namespace MI.UserManager.Web.Controllers
{
    [Route("api/[controller]")]
    public class PermissionTemplateController : Controller
    {
        public PermissionTemplateController()
        {

        }

        [HttpGet]
        public IActionResult GetAll()
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var permtemps = scope.Query<PermissionTemplate>().ToList();

                scope.Done();

                var ret = new List<PermissionViewModel>();

                foreach(var pt in permtemps)
                {
                    var pret = new List<ParameterViewModel>();
                    foreach (var ptp in pt.Parameters)
                    {
                        pret.Add(new ParameterViewModel(ptp.InternalId, ptp.Name, ptp.Description, ptp.Type.Name, ptp.DefaultValue, ptp.Type.InternalId));
                    }

                    ret.Add(new PermissionViewModel(pt.InternalId, pt.Name, pt.Description, pt.ClaimType, pret));
                }

                return new OkObjectResult(ret);
            }
        }

        [HttpGet(),
         Route("[action]/{userInternalId}")]
        public IActionResult GetFilteredByUserInternalId(Guid userInternalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var permIds = scope.Query<PermissionAssignment>().Where(pa => pa.InternalId == userInternalId)
                    .Select(pap => pap.PermissionTemplate.InternalId);

                var baseQuery = scope.Query<PermissionTemplate>()
                    .Where(ra => !permIds.Contains(ra.InternalId));

                baseQuery.FetchMany(p => p.Parameters).ThenFetch(p => p.Type).ToFuture();

                var result = baseQuery.ToFuture();

                scope.Done();

                var res = result.ToList();

                var permissionTemplates = new List<PermissionViewModel>();

                foreach (var permissionTemplate in res)
                {
                    permissionTemplates.Add(PermissionFactory.CreateNewPermissionViewModel(permissionTemplate));
                }

                return new OkObjectResult(permissionTemplates);
            }
        }

        [HttpGet("{internalId}")]
        public IActionResult GetByInternalId(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var baseQuery = scope.Query<PermissionTemplate>()
                    .Where(pt => pt.InternalId == internalId);
                
                baseQuery.FetchMany(p => p.Parameters).ThenFetch(p => p.Type).ToFuture();

                var res = baseQuery.ToFuture().ToList()?.First();

                scope.Done();

                if (res != null)
                {
                    var viewModel = new PermissionViewModel(res.InternalId, res.Name, res.ClaimType, res.Description);
                    var parameters = new List<ParameterViewModel>();

                    foreach (var fp in res.Parameters)
                    {
                        //ParameterTypeViewModel pm = new ParameterTypeViewModel(fp.Type.InternalId, fp.Type.Name, fp.Type.Description, fp.Type.TypeDiscriminator);
                        parameters.Add(new ParameterViewModel(fp.InternalId, fp.Name, fp.Description, fp.Type.Name ,fp.DefaultValue, fp.Type.InternalId));
                    }

                    viewModel.parameters = parameters;
                    return new OkObjectResult(viewModel);
                }
            }

            return NotFound();
        }

        [HttpGet(),
        Route("[action]/{roleInternalId}")]
        public IActionResult GetFilteredByRoleInternalId(Guid roleInternalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var rolePermIds = scope.Query<RoleTemplate>().FirstOrDefault(rt => rt.InternalId == roleInternalId)?
                    .PermissionTemplates.Select(rpt => rpt.InternalId);

                if (rolePermIds != null && rolePermIds.Any())
                {
                    var baseQuery = scope.Query<PermissionTemplate>()
                        .Where(pt => !rolePermIds.Contains(pt.InternalId));

                    baseQuery.FetchMany(p => p.Parameters).ThenFetch(p => p.Type).ToFuture();

                    var res = baseQuery.ToFuture().ToList();

                    scope.Done();

                    if (res.Any())
                    {
                        var viewModels = new List<PermissionViewModel>();
                        res.ForEach(r =>
                        {
                            var paramsList = r.Parameters.Select(ptParam => new ParameterViewModel(ptParam.InternalId,
                                ptParam.Name, ptParam.Description, ptParam.Type.Name, ptParam.DefaultValue,
                                ptParam.Type.InternalId)).ToList();

                            var pvm = new PermissionViewModel(r.InternalId, r.Name, r.ClaimType, r.Description)
                            {
                                parameters = paramsList
                            };

                            viewModels.Add(pvm);
                        });

                        return new OkObjectResult(viewModels);
                    }
                }
            }

            return NotFound("No unassigned permssions exist");
        }

        [HttpPost]
        public IActionResult Create([FromBody] PermissionViewModel permissionTemplate)
        {
            if (permissionTemplate == null)
                return BadRequest();

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var pt = new PermissionTemplate(permissionTemplate.name, permissionTemplate.claimType, permissionTemplate.description, Guid.NewGuid());

                var param = new HashSet<PermissionTemplateParameter>();

                //add new params for each viewmodel param in the template
                foreach(var p in permissionTemplate.parameters)
                {
                    var retType = scope.Query<ParameterType>().FirstOrDefault(ptype => ptype.InternalId == p.typeInternalId);
                    var addParam = new PermissionTemplateParameter(p.name, p.description, Guid.NewGuid(), retType, p.value);
                    addParam.ParentPermissionTemplate = pt;
                    param.Add(addParam);
                }

                pt.Parameters = param;

                scope.Persist(pt);    
                
                scope.Done();
            }

            return new NoContentResult();
        }

        [HttpPut("{internalId}")]
        public IActionResult Update(Guid internalId, [FromBody] PermissionViewModel permissionTemplate)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var existingTemplate = scope.Query<PermissionTemplate>().FirstOrDefault(r => r.InternalId == internalId);

                if (existingTemplate == null || permissionTemplate == null)
                    return NotFound();

                existingTemplate.Name = permissionTemplate.name;
                existingTemplate.ClaimType = permissionTemplate.claimType;
                existingTemplate.Description = permissionTemplate.description;

                var paramarray = new HashSet<PermissionTemplateParameter>();

                foreach(var p in permissionTemplate.parameters)
                {
                    var ptype = scope.Query<ParameterType>().FirstOrDefault(pt => pt.InternalId == p.typeInternalId);
                    var guid = Guid.NewGuid();

                    if (p.internalId != Guid.Empty && p.internalId != null)
                        guid = p.internalId;

                    paramarray.Add(new PermissionTemplateParameter(p.name, p.description, guid, ptype, p.value));
                }

                existingTemplate.Parameters = paramarray;

                scope.Update(existingTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }

        [HttpDelete("{internalId}")]
        public IActionResult Delete(Guid internalId)
        {
            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                var permissionTemplate = scope.Query<PermissionTemplate>().FirstOrDefault(r => r.InternalId == internalId);

                var permAssignments = scope.Query<PermissionAssignment>().Where(pa => pa.PermissionTemplate.InternalId == permissionTemplate.InternalId).ToList();

//                var permTemplateParams = scope.Query<PermissionTemplateParameter>().Where(ptp => ptp.ParentPermissionTemplate.InternalId == permissionTemplate.InternalId).ToList();

                foreach(var assignment in permAssignments)
                {
                    assignment.PermissionTemplate = null;
                }

                //foreach(var param in permTemplateParams)
                //{
                //    scope.Delete(param);
                //}

                scope.Delete(permissionTemplate);

                scope.Done();

                return new NoContentResult();
            }
        }
    }
}
