﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Flee.PublicTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using MI.UserManager.Web.Shared.Attributes;
using MI.UserManager.Web.Shared.Requirements;
using Newtonsoft.Json;

namespace MI.UserManager.Web.Handlers
{
    public class ControllerActionPermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        private readonly IHttpContextAccessor _context;

        public ControllerActionPermissionAuthorizationHandler(IHttpContextAccessor context)
        {
            _context = context;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            PermissionRequirement requirement)
        {
            var authorizationFilterContext = context.Resource as AuthorizationFilterContext;
            var actionDescriptor = authorizationFilterContext?.ActionDescriptor as ControllerActionDescriptor;
            var methodInfo = actionDescriptor?.MethodInfo;
            var requiresPermissionAttributes = methodInfo.GetCustomAttributes<RequirePermissionAttribute>().ToList();

            var failed = false;

            foreach (var rp in requiresPermissionAttributes)
            {
                //if failed previously then short circuit out of the List.foreach()
                if (failed) continue; 

                //find the permission value from the route params or querystring, looking first in route params then querystrings
                var param = string.Empty;
                if (!string.IsNullOrEmpty(_context.HttpContext.GetRouteData().Values[rp.PermissionName]?.ToString())
                )
                    param = _context.HttpContext.GetRouteData().Values[rp.PermissionName].ToString();
                else if (_context.HttpContext.Request.Query[rp.PermissionName].Any())
                    param = _context.HttpContext.Request.Query[rp.PermissionName][0];

                //if the user doesn't have the claim or the param doesn't exist in the route params or querystring then fail
                if (!context.User.HasClaim(c => c.Type.Equals(rp.PermissionName, StringComparison.InvariantCultureIgnoreCase)) || string.IsNullOrWhiteSpace(param))
                {
                    failed = true;
                    continue;
                }

                //find the claim and evaluate expression for permission
                var claim = context.User.FindFirst(c => c.Type.Equals(rp.PermissionName, StringComparison.InvariantCultureIgnoreCase));
                if (!EvaluateExpressionWithParams(param, claim.Value.Split(';').ToList()))
                    failed = true;
                //context.Succeed(requirement);
            }

            if (failed)
                return Task.CompletedTask;

            context.Succeed(requirement);
            return Task.CompletedTask;
        }

        private bool EvaluateExpressionWithParams(string permittedValue, List<string> tokenValues)
        {
            var context = new ExpressionContext();
            context.Imports.AddType(typeof(CustomFunctions));

            var sb = new StringBuilder();
            tokenValues.ForEach(tv => {
                sb.Append($"\"{tv.Substring(tv.IndexOf('=') + 1).Trim()}\"");
                if (tokenValues.IndexOf(tv) < tokenValues.Count - 1)
                    sb.Append(",");
            });

            var expression = $"ValidatePermissionToken(\"{ permittedValue }\", { sb })";

            var e = context.CompileDynamic(expression);
            return (bool)e.Evaluate();

        }

        private static Dictionary<string, string> ClaimJsonToDictionary(string claim)
            => JsonConvert.DeserializeObject<Dictionary<string, string>>(claim);

        public static class CustomFunctions
        {
            public static bool ValidatePermissionToken(string requiredPermission, params string[] args)
                => args.Any(param => requiredPermission.Equals(param.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }
    }

    public class BasicAuthorisationHandler : AuthorizationHandler<BasicRequirement>
    {
        public BasicAuthorisationHandler() { }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, BasicRequirement requirement)
        {
            //if the user doesn't have the claim or the param doesn't exist in the route params or querystring then fail
            //if (!context.User.Identity.IsAuthenticated)
            //{
            //    context.HasFailed();
            //}
            throw new NotImplementedException();
        }
    }
}
