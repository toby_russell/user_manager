﻿using System.Reflection;
using MI.Framework.Persistence;
using MI.Framework.Persistence.NHibernate;
using MI.UserManager.Persistence.Mapping;

namespace MI.UserManager.Persistence.Database
{
    public static class PostgresDatabase
    {
        public static void Connect(string hostServer, int port, string dbName, string user, string password,
            bool createNewDb, bool logSqlInConsole)
        {
            PostgresConfiguration.ConnectionString connectionString = new PostgresConfiguration.ConnectionString()
            {
                Host = hostServer,
                Port = port,
                Database = dbName,
                Username = user,
                Password = password,
                ClientEncoding = "UTF8"
            };
            
            PostgresConfiguration cfg = new PostgresConfiguration(connectionString, logSqlInConsole);

            //add mapping assembly for whatever
            cfg.AddMappingAssembly(typeof(OrganisationMapping).Assembly);
            cfg.AddMappingAssembly(Assembly.GetExecutingAssembly());
            cfg.CompileMappings();
            
            if(createNewDb) cfg.CreateDatabase(logSqlInConsole);

            PersistenceContext.SetProvider(new NHibernateProvider());
            NHibernateProvider.Configure(cfg);
        }
    }
}
