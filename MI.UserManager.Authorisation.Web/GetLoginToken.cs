﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using MI.Framework.Persistence;
using MI.UserManager.Model;

namespace MI.UserManager.Authorisation.Web
{
    public class GetLoginToken
    {
        public static TokenProviderOptions GetOptions()
        {
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.Config.GetSection("TokenAuthentication:SecretKey").Value));

            var expire =
                TimeSpan.FromMinutes(Convert.ToInt32(Configuration.Config
                    .GetSection("TokenAuthentication:ExpirationMinutes").Value));

            return new TokenProviderOptions
            {
                Path = Configuration.Config.GetSection("TokenAuthentication:TokenPath").Value,
                Audience = Configuration.Config.GetSection("TokenAuthentication:Audience").Value,
                Issuer = Configuration.Config.GetSection("TokenAuthentication:Issuer").Value,
                Expiration = expire,
                RefreshExpiration = TimeSpan.FromMinutes(Convert.ToInt32(Configuration.Config.GetSection("TokenAuthentication:RefreshExpirationMinutes").Value)),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
            };
        }

        public static LoginResponseData Execute(ApplicationUser user, RefreshToken refreshToken = null)
        {
            try
            {
                var options = GetOptions();
                var now = DateTime.UtcNow;

                var claims = new List<Claim>()
                {
                    new Claim(JwtRegisteredClaimNames.Iss, "ManufacturingIntelligence"),
                    new Claim(JwtRegisteredClaimNames.NameId, user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, user.InternalId.ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now).ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
                };

                using (var scope = new PersistenceScope())
                { 
                    scope.Attach(user);
                    var permissionAssignments = user.Permissions; 
                    foreach (var permission in permissionAssignments)
                    {
                        var i = 0;
                        var @params = permission.Parameters;
                        var sb = new StringBuilder();
                        foreach (var param in @params)
                        {
                            sb.Append($"{ param.Name }={ param.Value }");

                            if (++i < @params.Count)
                                sb.Append(";");
                        }

                        claims.Add(new Claim(permission.ClaimType, sb.ToString()));
                    }

                    var userRoles = user.Roles;
                    claims.AddRange(userRoles.Select(userRole => new Claim("role", userRole.Name)));

                    //delete old refresh tokens for user
                    var oldRefreshTokens = scope.Query<RefreshToken>().Where(rt => rt.User.UserId == user.UserId);
                    if(oldRefreshTokens.Any())
                        oldRefreshTokens.ToList().ForEach(rt => scope.Delete(rt));

                    //insert new refreeh token
                    refreshToken = new RefreshToken
                    {
                        User = user,
                        Token = Guid.NewGuid().ToString("N"),
                        IssuedUtc = now,
                        ExpiresUtc = now.Add(options.RefreshExpiration),
                    };
           
                    scope.Persist(refreshToken);
                

            

                    var jwt = new JwtSecurityToken(
                        issuer: options.Issuer,
                        audience: options.Audience,
                        claims: claims.ToArray(),
                        notBefore: now,
                        expires: now.Add(options.Expiration),
                        signingCredentials: options.SigningCredentials);
                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                    var response = new LoginResponseData
                    {
                        access_token = encodedJwt,
                        refresh_token = refreshToken.Token,
                        expires_in = (int)options.Expiration.TotalSeconds,
                        userName = user.Email,
                        firstName = user.FirstName,
                        lastName = user.LastName,
                        isAdmin = claims.Any(i => i.Type == "role" && i.Value == "admin")
                    };
                    scope.Done();
                    return response;

                }
            }
            catch (Exception e)
            {
                //need to rollback scope if ex thrown
                Console.WriteLine(e);
                throw;
            }
        }
    }

    public class TokenProviderOptions
    {
        /// <summary>
        /// The relative request path to listen on.
        /// </summary>
        /// <remarks>The default path is <c>/token</c>.</remarks>
        public string Path { get; set; } = "api/token";

        /// <summary>
        ///  The Issuer (iss) claim for generated tokens.
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// The Audience (aud) claim for the generated tokens.
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// The expiration time for the generated tokens.
        /// </summary>
        /// <remarks>The default is thirty minutes.</remarks>
        public TimeSpan Expiration { get; set; } = TimeSpan.FromMinutes(30);

        /// <summary>
        /// The signing key to use when generating tokens.
        /// </summary>
        public SigningCredentials SigningCredentials { get; set; }

        /// <summary>
        /// The expiration time for the refresh tokens.
        /// </summary>
        /// <remarks>The default is sixty minutes.</remarks>
        public TimeSpan RefreshExpiration { get; set; } = TimeSpan.FromMinutes(60);
    }

    public class LoginResponseData
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public int expires_in { get; set; }
        public string userName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public bool isAdmin { get; set; }
    }
}
