﻿using System;
using System.Linq;
using System.Text;
using MI.Framework.Persistence;
using MI.UserManager.Model;
using MI.UserManager.Web.Shared.Services;

namespace MI.UserManager.Authorisation.Web.Services
{
    public interface ILoginService
    {
        ApplicationUser Login(string email, string password);
        bool Logout(string username);
    }

    public class LoginService : ILoginService
    {
        private readonly IHashPasswords _hashPasswords;

        public LoginService(IHashPasswords hashPasswords)
        {
            _hashPasswords = hashPasswords;
        }

        public ApplicationUser Login(string email, string password)
        {
            try
            {
                using (var scope = new PersistenceScope(TransactionOption.Required))
                { 
                    var user = scope.Query<ApplicationUser>()
                        .FirstOrDefault(u => u.Email.Equals(email));
                    scope.Done();
                    if (user != null)
                    {
                        var decodedSalt = Convert.FromBase64String(user.PasswordSalt);
                        var hashed = _hashPasswords.HashPassword(Encoding.UTF8.GetBytes(password), decodedSalt);
                        var storedHash = user.PasswordHash;
                        if (!storedHash.Equals(hashed))
                            return null;
                    }
              
                    return user;
                    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool Logout(string username)
        {
            throw new NotImplementedException();
        }
    }
}
