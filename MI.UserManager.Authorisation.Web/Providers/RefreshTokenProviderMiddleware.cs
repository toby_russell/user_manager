﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MI.Framework.Persistence;
using MI.UserManager.Model;
using Newtonsoft.Json;

namespace MI.UserManager.Authorisation.Web.Providers
{
    public class RefreshTokenProviderMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly JsonSerializerSettings _serializerSettings;

        public RefreshTokenProviderMiddleware(RequestDelegate next)
        {
            _next = next;

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }


        public Task Invoke(HttpContext context)
        {
            // If the request path doesn't match, skip
            if (!context.Request.Path.Equals("/api/refresh", StringComparison.Ordinal))
            {
                return _next(context);
            }

            // Request must be POST with Content-Type: application/x-www-form-urlencoded
            if (!context.Request.Method.Equals("POST")
               || !context.Request.HasFormContentType)
            {
                context.Response.StatusCode = 400;
                return context.Response.WriteAsync("Bad request.");
            }


            return GenerateToken(context);
        }

        private async Task GenerateToken(HttpContext context)
        {
            try
            {
                var refreshToken = context.Request.Form["refreshToken"].ToString();
                if (string.IsNullOrWhiteSpace(refreshToken))
                {
                    context.Response.StatusCode = 400;
                    await context.Response.WriteAsync("User must relogin.");
                    return;
                }

                using (var scope = new PersistenceScope())
                {
                    var refreshTokenModel = scope.Query<RefreshToken>()
                        .SingleOrDefault(i => i.Token == refreshToken);

                    if (refreshTokenModel == null || refreshTokenModel.ExpiresUtc <= DateTime.UtcNow)
                    {
                        context.Response.StatusCode = 400;
                        await context.Response.WriteAsync("User must relogin.");
                        return;
                    }

                    var user = refreshTokenModel.User;
                    var token = GetLoginToken.Execute(user, refreshTokenModel);
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(token, _serializerSettings));
                    scope.Done();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
 
        }


    }


}
