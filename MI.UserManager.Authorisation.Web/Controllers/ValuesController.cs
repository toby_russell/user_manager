﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MI.UserManager.Authorisation.Web.Services;

namespace MI.UserManager.Authorisation.Web.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly ILoginService _loginService;

        public ValuesController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        // GET api/values
        [HttpGet,
         Authorize()]
        public IEnumerable<string> Get()
        {
            var user = _loginService.Login("luke.sandells@manufacturingintelligence.com.au", "password123");
            return new [] { user?.Email ?? "No user found!" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }


        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
