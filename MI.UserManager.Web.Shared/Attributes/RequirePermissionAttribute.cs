﻿using System;

namespace MI.UserManager.Web.Shared.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class RequirePermissionAttribute : Attribute
    {
        public string PermissionName { get; }

        public RequirePermissionAttribute(string permissionName)
        {
            PermissionName = permissionName;
        }

    }
}
