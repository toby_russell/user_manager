﻿using System;

namespace MI.UserManager.Web.Shared.Exceptions
{
    public class OrganisationDoesNotExistException : Exception
    {
        public OrganisationDoesNotExistException(Guid userVmOrganisationId): base($"An Organisation with the Internal Id {userVmOrganisationId} cannot be found") {}
    }
}