﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace MI.UserManager.Web.Shared.Services
{
    public interface IHashPasswords
    {
        string HashPassword(byte[] password, byte[] salt);
        byte[] CreateSalt(int size);
    }

    public class HashPasswordsService : IHashPasswords
    {
        /// <summary>
        /// This method salts and hashes passwords for storage in the ApplicationUser.Password property/db column
        /// </summary>
        /// <param name="password">Thi is the plain text passwrod to be salted and hashed</param>
        /// <param name="salt">The salt used</param>
        /// <returns>hashed password converted to a string from a byte array</returns>
        public string HashPassword(byte[] password, byte[] salt)
        {
            try
            {
                var hashBytes = PlaintextToSaltedHash(password, salt);
                var str = Convert.ToBase64String(hashBytes);
                return str;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public byte[] CreateSalt(int size)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buf = new byte[size];
            rng.GetBytes(buf);

            return buf;
        }

        private static byte[] PlaintextToSaltedHash(byte[] plainText, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            byte[] plainAndSalt = new byte[plainText.Length + salt.Length];

            int i = 0;
            foreach (byte c in plainText)
            {
                plainAndSalt[i] = c;
                i++;
            }

            foreach (byte c in salt)
            {
                plainAndSalt[i] = c;
                i++;
            }

            return algorithm.ComputeHash(plainAndSalt);
        }
    }
}
