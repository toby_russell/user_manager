﻿using System;
using System.Collections.Generic;
using MI.UserManager.Model;

namespace MI.UserManager.Web.Shared.Services
{
    public interface IUserService
    {
        void SendConfirmEmailAddressAndChangePasswordEmail(string newUserEmail);
    }
}