﻿using System;
using System.Collections.Generic;

namespace MI.UserManager.Web.Shared.Models
{
    public class RoleViewModel
    {
        public Guid internalId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public IEnumerable<ParameterViewModel> parameters { get; set; }
        public IEnumerable<PermissionViewModel> permissions { get; set; }

        public RoleViewModel() { }

        public RoleViewModel(Guid internalId, string name, string description)
        {
            this.internalId = internalId;
            this.name = name;
            this.description = description;
        }

        public RoleViewModel(Guid internalId, string name, string description, IEnumerable<ParameterViewModel> paramarray)
        {
            this.internalId = internalId;
            this.name = name;
            this.description = description;
            this.parameters = paramarray;
        }

        public RoleViewModel(Guid internalId, string name, string description, IEnumerable<ParameterViewModel> paramarray, IEnumerable<PermissionViewModel> perms)
        {
            this.internalId = internalId;
            this.name = name;
            this.description = description;
            this.parameters = paramarray;
            this.permissions = perms;
        }
    }
}