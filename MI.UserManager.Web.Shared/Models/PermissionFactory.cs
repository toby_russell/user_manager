﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.UserManager.Model;

namespace MI.UserManager.Web.Shared.Models
{
    public class PermissionFactory
    {
        public static PermissionViewModel CreateNewPermissionViewModel(PermissionTemplate permissionTemplate)
        {
            var parameters = new List<ParameterViewModel>();

            foreach(var parameter in permissionTemplate.Parameters)
            {
                parameters.Add(new ParameterViewModel(parameter.InternalId, parameter.Name, parameter.Description, parameter.Type.Name, parameter.DefaultValue, parameter.Type.InternalId));
            }

            return new PermissionViewModel(permissionTemplate.InternalId, permissionTemplate.Name, permissionTemplate.Description, permissionTemplate.ClaimType, parameters);
        }

        public static PermissionViewModel CreateNewPermissionViewModel(PermissionAssignment permissionAssignment)
        {
            var parameters = new List<ParameterViewModel>();

            foreach (var parameter in permissionAssignment.Parameters)
            {
                parameters.Add(new ParameterViewModel(parameter.InternalId, parameter.Name, parameter.Description, parameter.Type.Name, parameter.Value, parameter.Type.InternalId));
            }

            return new PermissionViewModel(permissionAssignment.InternalId, permissionAssignment.Name, permissionAssignment.Description, permissionAssignment.ClaimType, parameters);
        }
    }
}
