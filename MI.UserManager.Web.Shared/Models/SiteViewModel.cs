﻿using System;

namespace MI.UserManager.Web.Shared.Models
{
    public class SiteViewModel
    {
        public Guid InternalId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}