﻿using System;
using System.Collections.Generic;

namespace MI.UserManager.Web.Shared.Models
{
    public class ApplicationUserViewModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public bool isEnabled { get; set; }
        public string email { get; set; }
        //public string password { get; set; }
        public Guid internalId { get; set; }
        public Guid organisationId { get; set; }

        public IEnumerable<RoleViewModel> roles { get; set; }
        public IEnumerable<PermissionViewModel> permissions { get; set; }
        //public IEnumerable<ParameterViewModel> parameters { get; set; }

        public ApplicationUserViewModel()
        {
        }

        public ApplicationUserViewModel(string firstName, string lastName, bool isEnabled, string email, Guid internalId, Guid organisationId/*, string password = null*/)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.isEnabled = isEnabled;
            this.email = email;
            this.internalId = internalId;
            this.organisationId = organisationId;
            //this.password = password;
        }

        public ApplicationUserViewModel(string firstName, string lastName, bool isEnabled, string email, Guid internalId, Guid organisationId, IEnumerable<RoleViewModel> rolearray, IEnumerable<PermissionViewModel> permarray, IEnumerable<ParameterViewModel> paramarray/*, string password = null*/)
            : this(firstName, lastName, isEnabled, email, internalId, organisationId/*, password*/)
        {
            roles = rolearray;
            permissions = permarray;
            //parameters = paramarray;
        }
    }
}
