﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UserManager.Web.Shared.Models
{
    public class ParameterTypeViewModel
    {
        public Guid InternalId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TypeDiscriminator { get; set; }

        public ParameterTypeViewModel(Guid internalId, string name, string description, string typeDiscriminator)
        {
            InternalId = internalId;
            name = Name;
            Description = description;
            TypeDiscriminator = typeDiscriminator;
        }
    }
}
