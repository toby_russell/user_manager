﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.UserManager.Model;

namespace MI.UserManager.Web.Shared.Models
{
    public class ApplicationUserFactory
    {
        public static ApplicationUserViewModel CreateNewApplicationUserViewModel(ApplicationUser applicationUser)
        {
            var permissions = new List<PermissionViewModel>();
            var roles = new List<RoleViewModel>();
            var parameters = new List<ParameterViewModel>();

            foreach(var role in applicationUser.Roles)
            {
                roles.Add(RoleFactory.CreateNewRoleViewModel(role));
                //foreach(var param in role.Parameters)
                //{
                //    parameters.Add(new ParameterViewModel(param.InternalId, param.Name, param.Description, param.Discriminator, param.Type.Name, param.Value, param.Type.InternalId));
                //}
            }

            foreach(var perm in applicationUser.Permissions)
            {
                permissions.Add(PermissionFactory.CreateNewPermissionViewModel(perm));
                //foreach (var param in perm.Parameters)
                //{
                //    parameters.Add(new ParameterViewModel(param.InternalId, param.Name, param.Description, param.Discriminator, param.Type.Name, param.Value, param.Type.InternalId));
                //}
            }

            return new ApplicationUserViewModel(applicationUser.FirstName, applicationUser.LastName, applicationUser.IsEnabled, applicationUser.Email, applicationUser.InternalId, applicationUser.Organisation.InternalId, roles, permissions, parameters);
        }
    }
}
