﻿using System;

namespace MI.UserManager.Web.Shared.Models
{
    public class OrganisationViewModel
    {
        public Guid InternalId { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
    }
}
