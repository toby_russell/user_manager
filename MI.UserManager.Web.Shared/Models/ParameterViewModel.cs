﻿using System;

namespace MI.UserManager.Web.Shared.Models
{
    public class ParameterViewModel
    {
        public Guid internalId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        //public string discriminator { get; set; }
        public string typeName { get; set; }
        public string value { get; set; }
        public Guid typeInternalId { get; set; }

        public ParameterViewModel() { }

        public ParameterViewModel(Guid internalId, string name, string description, string typeName, string value, Guid typeInternalId)
        {
            this.internalId = internalId;
            this.name = name;
            this.description = description;
            //this.discriminator = discriminator;
            this.typeName = typeName;
            this.value = value;
            this.typeInternalId = typeInternalId;
        }
    }
}