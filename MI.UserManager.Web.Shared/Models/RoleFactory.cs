﻿using System;
using System.Collections.Generic;
using System.Text;
using MI.UserManager.Model;

namespace MI.UserManager.Web.Shared.Models
{
    public class RoleFactory
    {
        public static RoleViewModel CreateNewRoleViewModel(RoleTemplate roleTemplate)
        {
            var parameters = new List<ParameterViewModel>();
            var permissions = new List<PermissionViewModel>();

            foreach (var prm in roleTemplate.Parameters)
            {
                parameters.Add(new ParameterViewModel(prm.InternalId, prm.Name, prm.Description, prm.Type.Name, prm.DefaultValue, prm.Type.InternalId));
            }

            foreach (var pt in roleTemplate.PermissionTemplates)
            {
                permissions.Add(PermissionFactory.CreateNewPermissionViewModel(pt));
            }

            //ptret.ForEach(@param => prmret.AddRange(@param.parameters));

            return new RoleViewModel(roleTemplate.InternalId, roleTemplate.Name, roleTemplate.Description, parameters, permissions);
        }

        public static RoleViewModel CreateNewRoleViewModel(RoleAssignment roleAssignment)
        {
            var parameters = new List<ParameterViewModel>();
            var permissions = new List<PermissionViewModel>();

            foreach (var prm in roleAssignment.Parameters)
            {
                parameters.Add(new ParameterViewModel(prm.InternalId, prm.Name, prm.Description, prm.Type.Name, prm.Value, prm.Type.InternalId));
            }

            foreach (var pt in roleAssignment.PermissionAssignments)
            {
                permissions.Add(PermissionFactory.CreateNewPermissionViewModel(pt));
            }

            //ptret.ForEach(@param => prmret.AddRange(@param.parameters));

            return new RoleViewModel(roleAssignment.InternalId, roleAssignment.Name, roleAssignment.Description, parameters, permissions);
        }

        //public static RoleTemplate CreateNewRoleTemplate(RoleViewModel RoleViewModel)
        //{
        //    Guid guid = RoleViewModel.internalId;

        //    if (RoleViewModel.internalId == null || RoleViewModel.internalId == Guid.Empty)
        //        guid = Guid.NewGuid();

        //    var paramList = new List<Parameter>();

        //    foreach(var prm in RoleViewModel.parameters)
        //    {
        //       // paramList.Add(new Parameter(prm.name, prm.description, prm.internalId, prm.type));
        //    }

        //    return new RoleTemplate(RoleViewModel.name, RoleViewModel.description, guid);
        //}
    }
}
