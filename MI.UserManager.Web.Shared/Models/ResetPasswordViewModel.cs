﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UserManager.Web.Shared.Models
{
    public class ResetPasswordViewModel
    {
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string confirmNewPassword { get; set; }
    }
}
