﻿using System;
using System.Collections.Generic;

namespace MI.UserManager.Web.Shared.Models
{
    public class PermissionViewModel
    {
        /*
    name: string;
    internalId: string;
    description: string;
    claimType: string;
    parameters: Parameter[];
         * 
         * */
        public Guid internalId { get; set; }
        public string name { get; set; }
        public string claimType { get; set; }
        public string description { get; set; }
        public IEnumerable<ParameterViewModel> parameters { get; set; }

        public PermissionViewModel() { }

        public PermissionViewModel(Guid internalId, string name, string claimType, string description)
        {
            this.internalId = internalId;
            this.name = name;
            this.claimType = claimType;
            this.description = description;
        }

        public PermissionViewModel(Guid internalId, string name, string description, string claimType, IEnumerable<ParameterViewModel> paramList)
        {
            this.internalId = internalId;
            this.name = name;
            this.description = description;
            this.claimType = claimType;
            this.parameters = paramList;
        }
    }
}