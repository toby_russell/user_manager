﻿using MI.UserManager.Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.UserManager.Persistence.Mapping
{
    public class RoleAssignmentMapping : ClassMapping<RoleAssignment>
    {
        public RoleAssignmentMapping()
        {
            Id(ra => ra.RoleAssignmentId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, gen => gen.Params(new { max_lo = 1000 }));
            });

            Property(ra => ra.Name, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(ra => ra.Description, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(ra => ra.InternalId, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });

            //this is provided for reporting rather than operational use  
            ManyToOne(ra => ra.RoleTemplate, manyToOne =>
            {
                manyToOne.Column("RoleTemplateId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(false);
                manyToOne.Cascade(Cascade.Persist);
                manyToOne.Update(true);
            });

            Set(ra => ra.PermissionAssignments, set =>
            {
                set.Inverse(false);
                set.Cascade(Cascade.Persist | Cascade.DeleteOrphans);
                set.Key(k => k.Column("RoleAssignmentId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
            }, rel => rel.OneToMany());

            Set(ra => ra.Parameters, set =>
            {
                set.Inverse(false);
                set.Key(k => k.Column("RoleAssignmentId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
                set.Cascade(Cascade.Persist | Cascade.DeleteOrphans);
            }, rel => rel.OneToMany());

            //I was sure I changed this...
            //Set(ra => ra.Parameters, set =>
            //{
            //    set.Table("RoleAssignmentRoleParameter");
            //    set.Key(k => k.Column("RoleAssignmentId"));
            //    set.Access(Accessor.Property);
            //    set.Fetch(CollectionFetchMode.Select);
            //}, rel => rel.ManyToMany(r =>
            //{
            //    r.Column("ParameterId");
            //}));
        }
    }
}
