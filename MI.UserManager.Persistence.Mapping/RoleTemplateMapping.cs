﻿using MI.UserManager.Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.UserManager.Persistence.Mapping
{
    public class RoleTemplateMapping : ClassMapping<RoleTemplate>
    {
        public RoleTemplateMapping()
        {
            Id(rt => rt.RoleTemplateId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });
            
            Property(rt => rt.InternalId, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(rt => rt.Name, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });
            Property(rt => rt.Description, prop => { prop.Access(Accessor.Property); prop.NotNullable(true); });

            //Set(rt => rt.PermissionTemplates, set =>
            //{
            //    set.Table("RoleTemplatePermissionTemplate");
            //    set.Key(k => k.Column("PermissionTemplateId"));
            //    set.Access(Accessor.Property);
            //    set.Fetch(CollectionFetchMode.Select);
            //}, rel => rel.ManyToMany(r =>
            //{
            //    r.Column("RoleTemplateId");
            //}));

            //Set(rt => rt.ParameterTemplates, set =>
            //{
            //    set.Table("RoleTemplateParameterTemplate");
            //    set.Key(k => k.Column("RoleTemplateId"));
            //    set.Access(Accessor.Property);
            //    set.Fetch(CollectionFetchMode.Select);
            //}, rel => rel.ManyToMany(r =>
            //{
            //    r.Column("ParameterId");
            //}));

            Set(rt => rt.PermissionTemplates, set =>
            {
                set.Inverse(false);
                set.Cascade(Cascade.Persist);
                set.Key(k => k.Column("RoleTemplateId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
            }, ret => ret.OneToMany());

            Set(rt => rt.Parameters, set =>
            {
                set.Inverse(false);
                set.Cascade(Cascade.Persist);
                set.Key(k => k.Column("RoleTemplateId"));
                set.Access(Accessor.Property);
                set.Fetch(CollectionFetchMode.Select);
            }, rel => rel.OneToMany());
        }
    }
}
