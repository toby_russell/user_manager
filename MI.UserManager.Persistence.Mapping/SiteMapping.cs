﻿using MI.UserManager.Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.UserManager.Persistence.Mapping
{
    public class SiteMapping : ClassMapping<Site>
    {
        public SiteMapping()
        {
            Id(s => s.SiteId, id =>
            {
                id.Access(Accessor.Property);
                id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
            });

            Property(s => s.Name, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(s => s.Description, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });
            Property(s => s.InternalId, prop => { prop.NotNullable(true); prop.Access(Accessor.Property); });

            ManyToOne(s => s.Organisation, manyToOne =>
            {
                manyToOne.Column("OrganisationId");
                manyToOne.Access(Accessor.Property);
                manyToOne.Fetch(FetchKind.Select);
                manyToOne.Lazy(LazyRelation.NoLazy);
                manyToOne.NotNullable(true);
                manyToOne.Cascade(Cascade.None);
            });
        }
    }
}
