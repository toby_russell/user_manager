﻿using MI.UserManager.Model;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.UserManager.Persistence.Mapping
{
    public class StringParameterTypeMapping : SubclassMapping<StringParameterType>
    {
        public StringParameterTypeMapping()
        {
            DiscriminatorValue("StringParameterType");
        }
    }
}