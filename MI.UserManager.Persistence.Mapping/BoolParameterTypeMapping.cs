﻿using MI.UserManager.Model;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.UserManager.Persistence.Mapping
{
    public class BoolParameterTypeMapping : SubclassMapping<BoolParameterType>
    {
        public BoolParameterTypeMapping()
        {
            DiscriminatorValue("BoolParameterType");

        }
    }
}