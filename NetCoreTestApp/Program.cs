﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using MI.Framework.Persistence;
using MI.UserManager.Model;
using MI.UserManager.Model.Factories;
using MI.UserManager.Persistence.Database;

namespace MI.AuthenticationTest.NetCoreTestApp
{
    class Program
    {
        static byte[] CreateSalt(int size)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buf = new byte[size];
            rng.GetBytes(buf);

            return buf;
        }

        static byte[] PlaintextToSaltedHash(byte[] plainText, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();
            byte[] plainAndSalt = new byte[plainText.Length + salt.Length];

            int i = 0;
            foreach (byte c in plainText)
            {
                plainAndSalt[i] = c;
                i++;
            }

            foreach (byte c in salt)
            {
                plainAndSalt[i] = c;
                i++;
            }

            return algorithm.ComputeHash(plainAndSalt);
        }

        static void Main(string[] args)
        {
            var orgsMap = new Dictionary<string, Organisation>();
            //List<Organisation> orgsList = new List<Organisation>();
            List<Site> sitesList = new List<Site>();
            List<ApplicationUser> usersList = new List<ApplicationUser>();
            var paramTypesMap = new Dictionary<string, ParameterType>();
            //List<ParameterType> paramTypesList = new List<ParameterType>();

            //List<Parameter> paramList = new List<Parameter>();
            var paramMap = new Dictionary<string, Parameter>();

            var roleTemplatesMap = new Dictionary<string, RoleTemplate>();
            //List<RoleTemplate> roleTemplatesList = new List<RoleTemplate>();
            var permTemplatesMap = new Dictionary<string, PermissionTemplate>();
            //List<PermissionTemplate> permTemplatesList = new List<PermissionTemplate>();

            List<RoleAssignment> roleAssignmentsList = new List<RoleAssignment>();
            List<PermissionAssignment> permAssignmentsList = new List<PermissionAssignment>();

            //general parameter types
            paramTypesMap["Site"] = new SiteParameterType("SiteParam", "Parameter of type site", Guid.NewGuid());
            paramTypesMap["String"] = new StringParameterType("StringParam", "Parameter of type string", Guid.NewGuid());
            paramTypesMap["Bool"] = new BoolParameterType("BoolParam", "Parameter of type bool", Guid.NewGuid());

            //test orgs
            orgsMap["MI"] = new Organisation("Manufacturing Intelligence", "manufacturingintelligence.com.au", Guid.NewGuid());
            orgsMap["FMG"] = new Organisation("Fortescue Metals Group", "fmg.com.au", Guid.NewGuid());
            orgsMap["S32"] = new Organisation("South32", "s32.com.au", Guid.NewGuid());

            //test sites
            sitesList.Add(new Site("Perth Office", "Perth CBD HQ", Guid.NewGuid(), orgsMap["MI"]));
            sitesList.Add(new Site("Solomon", "Solomon hub; contains Firetail pit and Kings pit", Guid.NewGuid(), orgsMap["FMG"]));
            sitesList.Add(new Site("Christmas Creek", "Christmas Creek hub", Guid.NewGuid(), orgsMap["FMG"]));
            sitesList.Add(new Site("Worsley", "Worsley hub", Guid.NewGuid(), orgsMap["S32"]));
            sitesList.Add(new Site("Saddleback", "Saddleback hub", Guid.NewGuid(), orgsMap["S32"]));
            sitesList.Add(new Site("Marradong", "Marradong hub", Guid.NewGuid(), orgsMap["S32"]));

            var password = new byte[256];
            password = System.Text.Encoding.UTF8.GetBytes("password123");
            var salt = CreateSalt(password.Length);
            var saltPw = PlaintextToSaltedHash(password, salt);
            string saltStr = Convert.ToBase64String(salt);
            string saltPwStr = Convert.ToBase64String(saltPw);

            //permission template parameters
            paramMap["Access1"] = new PermissionTemplateParameter("Access Parameter", "Allow or disallow access", Guid.NewGuid(), paramTypesMap["Bool"], "*");
            paramMap["Access2"] = new PermissionTemplateParameter("Access Parameter", "Allow or disallow access", Guid.NewGuid(), paramTypesMap["Bool"], "*");
            paramMap["Edit"] = new PermissionTemplateParameter("Edit parameter", "Allow or disallow editing", Guid.NewGuid(), paramTypesMap["Bool"], "*");
            paramMap["Site1"] = new PermissionTemplateParameter("Site parameter", "Allow access to a specified site", Guid.NewGuid(), paramTypesMap["Site"], "*");
            paramMap["Site2"] = new PermissionTemplateParameter("Site parameter", "Allow access to a specified site", Guid.NewGuid(), paramTypesMap["Site"], "*");
            paramMap["Banned"] = new PermissionTemplateParameter("Banned parameter", "State of ban", Guid.NewGuid(), paramTypesMap["Bool"], "*");

            //permission templates
            permTemplatesMap["EditSite"] = new PermissionTemplate("Edit site", "Site&Edit&Access", "Allows/disallows editing of specified site", Guid.NewGuid(), 
                new HashSet<PermissionTemplateParameter>() { (PermissionTemplateParameter)paramMap["Access1"], (PermissionTemplateParameter)paramMap["Edit"], (PermissionTemplateParameter)paramMap["Site1"] });
            permTemplatesMap["AccessSite"] = new PermissionTemplate("Access site", "Site&Access", "Allows/disallows access of specified site", Guid.NewGuid(),
                new HashSet<PermissionTemplateParameter>() { (PermissionTemplateParameter)paramMap["Access2"], (PermissionTemplateParameter)paramMap["Site2"] });
            permTemplatesMap["BanState"] = new PermissionTemplate("Ban state", "ban", "Determines whether user is banned", Guid.NewGuid(),
                new HashSet<PermissionTemplateParameter>() { (PermissionTemplateParameter)paramMap["Banned"] });

            //TODO: add a constructor without RoleTemplateParameter that can build them from the given PermissionTemplateParameters
            //role templates
            roleTemplatesMap["SiteEditor"] = new RoleTemplate("Site editor", "Can/cannot edit a given site", Guid.NewGuid(), 
                new HashSet<PermissionTemplate>() { permTemplatesMap["EditSite"] });
            roleTemplatesMap["SiteAccessor"] = new RoleTemplate("Site accessor", "Can/cannot access a given site", Guid.NewGuid(), 
                new HashSet<PermissionTemplate>() { permTemplatesMap["AccessSite"] });

            //TODO: Use the factory to make these with the templates from above
            //MI users
            usersList.Add(new ApplicationUser("Luke", "Sandells", true, "luke.sandells@manufacturingintelligence.com.au", Guid.NewGuid(), orgsMap["MI"], saltPwStr, saltStr));
            usersList.Add(new ApplicationUser("Toby", "Russell", true, "toby.russell@manufacturingintelligence.com.au", Guid.NewGuid(), orgsMap["MI"], saltPwStr, saltStr));
            //FMG users
            //usersList.Add(new ApplicationUser("Super", "FMG", true, "super@fmgsite.com.au", Guid.NewGuid(), orgsMap["FMG"], saltPwStr, saltStr,
            //    new HashSet<PermissionAssignment>() { PermissionAssignmentFactory.CreateNewPermissionAssignment(permTemplatesMap["BanState"]) },
            //    new HashSet<RoleAssignment>() { RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteEditor"]), RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteAccessor"]) }));
            //usersList.Add(new ApplicationUser("Editor", "FMG", true, "editor@fmgsite.com.au", Guid.NewGuid(), orgsMap["FMG"], saltPwStr, saltStr, null,
            //    new HashSet<RoleAssignment>() { RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteEditor"]) }));
            //usersList.Add(new ApplicationUser("Reader", "FMG", true, "reader@fmgsite.com.au", Guid.NewGuid(), orgsMap["FMG"], saltPwStr, saltStr, null,
            //    new HashSet<RoleAssignment>() { RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteAccessor"]) }));
            //usersList.Add(new ApplicationUser("Limited", "FMG", true, "limited@fmgsite.com.au", Guid.NewGuid(), orgsMap["FMG"], saltPwStr, saltStr, null,
            //    new HashSet<RoleAssignment>() { RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteEditor"]) }));
            ////South32 users
            //usersList.Add(new ApplicationUser("Editor", "South32", true, "editor@s32site.com.au", Guid.NewGuid(), orgsMap["S32"], saltPwStr, saltStr, null,
            //    new HashSet<RoleAssignment>() { RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteEditor"]) }));
            //usersList.Add(new ApplicationUser("Reader", "South32", true, "reader@s32site.com.au", Guid.NewGuid(), orgsMap["S32"], saltPwStr, saltStr, null,
            //    new HashSet<RoleAssignment>() { RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteAccessor"]) }));
            //usersList.Add(new ApplicationUser("Limited", "South32", true, "limited@s32site.com.au", Guid.NewGuid(), orgsMap["S32"], saltPwStr, saltStr, null,
            //    new HashSet<RoleAssignment>() { RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesMap["SiteEditor"]) }));

            PostgresDatabase.Connect("localhost", 5432, "MI.AuthenticationTest", "postgres", "manint", true, true);

            using (var scope = new PersistenceScope(TransactionOption.RequiresNew))
            {
                foreach (var o in orgsMap)
                {
                    scope.Persist(o.Value);
                }

                scope.Done();
            }

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                foreach (var s in sitesList)
                {
                    scope.Persist(s);
                }

                scope.Done();
            }

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                foreach (var pt in paramTypesMap)
                {
                    scope.Persist(pt.Value);
                }

                scope.Done();
            }

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                foreach (var pt in permTemplatesMap)
                {
                    scope.Persist(pt.Value);
                }

                scope.Done();
            }


            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                foreach(var rt in roleTemplatesMap)
                {
                    scope.Persist(rt.Value);
                }

                scope.Done();
            }

            using (var scope = new PersistenceScope(TransactionOption.Required))
            {
                foreach (var user in usersList)
                {
                    scope.Persist(user);
                    scope.Done();
                }
            }

                //using (var scope = new PersistenceScope(TransactionOption.Required))
                //{
                //    foreach (var p in paramList)
                //    {
                //        scope.Persist(p);
                //    }

                //    scope.Done();
                //}

                //using (var scope = new PersistenceScope(TransactionOption.Required))
                //{
                //    foreach (var pt in permTemplatesList)
                //    {
                //        scope.Persist(pt);
                //    }

                //    scope.Done();
                //}

                //using (var scope = new PersistenceScope(TransactionOption.Required))
                //{
                //    foreach (var rt in roleTemplatesList)
                //    {
                //        scope.Persist(rt);
                //    }

                //    scope.Done();
                //}

                //using (var scope = new PersistenceScope(TransactionOption.Required))
                //{
                //    foreach (var pa in permAssignmentsList)
                //    {
                //        scope.Persist(pa);
                //    }

                //    scope.Persist(permissionAssignment1);

                //    scope.Done();
                //}

                //using (var scope = new PersistenceScope(TransactionOption.Required))
                //{
                //    foreach (var ra in roleAssignmentsList)
                //    {
                //        scope.Persist(ra);
                //    }

                //    scope.Persist(roleAssignment1);

                //    scope.Done();
                //}

                ////params were here...

                //using (var scope = new PersistenceScope(TransactionOption.Required))
                //{
                //    //foreach (var u in usersList)
                //    //{
                //    //    scope.Persist(u);
                //    //}

                //    scope.Persist(usersList[0]);
                //    scope.Done();
                //}

                //using (var scope = new PersistenceScope(TransactionOption.Required))
                //{
                //    scope.Persist(tok);

                //    scope.Done();
                //}

                Console.WriteLine();

            //get things out and print
            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var orgList = scope.ListAll<Organisation>();
                foreach (Organisation o in orgList)
                {
                    Console.WriteLine($"{o.Name} {o.Domain} : {o.OrganisationId} {o.InternalId}");
                }

                scope.Done();
            }

            Console.WriteLine();

            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var usrList = scope.ListAll<ApplicationUser>();
                foreach (ApplicationUser u in usrList)
                {
                    Console.WriteLine($"{u.FirstName} {u.LastName} {u.Email} {u.Organisation.OrganisationId} {u.IsEnabled} : {u.UserId} {u.InternalId}");
                }

                scope.Done();
            }

            Console.WriteLine();

            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var sitesret = scope.ListAll<Site>();
                foreach (Site s in sitesret)
                {
                    Console.WriteLine($"{s.Name} {s.Description} : {s.InternalId}");
                }

                scope.Done();
            }

            Console.WriteLine();

            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var sitePermParamRet = scope.ListAll<SiteParameterType>();

                scope.Done();
            }

            Console.WriteLine();

            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var roleTempParamRet = scope.ListAll<RoleTemplateParameter>();

                scope.Done();
            }

            Console.WriteLine();

            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var permTempRet = scope.ListAll<PermissionTemplate>();

                scope.Done();
            }

            Console.WriteLine();

            using (var scope = new PersistenceScope(TransactionOption.None))
            {
                var roleTemplateRet = scope.ListAll<RoleTemplate>().ToList();

                scope.Done();
            }

            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}

//ORIGINAL LOAD DATA FOLLOWS

////test parameter types - for now just a SiteParameterType
//paramTypesList.Add(new SiteParameterType("SiteParam", "Parameter of type site", Guid.NewGuid()));
//paramTypesList.Add(new StringParameterType("StringParam", "Parameter of type string", Guid.NewGuid()));
//paramTypesList.Add(new BoolParameterType("BoolParam", "Parameter of type bool", Guid.NewGuid()));

////test some Parameter children
//paramList.Add(new RoleTemplateParameter("Site", "Site parameter", Guid.NewGuid(), paramTypesList[0], "*"));
//paramList.Add(new RoleTemplateParameter("Read", "Read parameter", Guid.NewGuid(), paramTypesList[2], "*"));
//paramList.Add(new RoleTemplateParameter("Write", "Write parameter", Guid.NewGuid(), paramTypesList[2], "*"));

//paramList.Add(new PermissionTemplateParameter("Read ZZZ", "Read parameter", Guid.NewGuid(), paramTypesList[2], "*"));
//paramList.Add(new PermissionTemplateParameter("Write", "Write parameter", Guid.NewGuid(), paramTypesList[2], "*"));
//paramList.Add(new PermissionTemplateParameter("Site", "Site parameter", Guid.NewGuid(), paramTypesList[0], "*"));
//paramList.Add(new PermissionTemplateParameter("Create", "Create parameter", Guid.NewGuid(), paramTypesList[2], "*"));

//paramList.Add(new RoleAssignmentParameter("Read", "Read role parameter", Guid.NewGuid(), paramTypesList[2], "true"));
//paramList.Add(new RoleAssignmentParameter("Read", "Read role parameter", Guid.NewGuid(), paramTypesList[2], "true"));
//paramList.Add(new RoleAssignmentParameter("Write", "Write role parameter", Guid.NewGuid(), paramTypesList[2], "write"));
//paramList.Add(new RoleAssignmentParameter("Site", "Site role parameter", Guid.NewGuid(), paramTypesList[0], sitesList[0].InternalId.ToString()));
//paramList.Add(new RoleAssignmentParameter("Site", "Site role parameter", Guid.NewGuid(), paramTypesList[0], sitesList[0].InternalId.ToString()));

//paramList.Add(new PermissionAssignmentParameter("Read", "Read permission parameter", Guid.NewGuid(), paramTypesList[2], "true"));
//paramList.Add(new PermissionAssignmentParameter("Read", "Read permission parameter", Guid.NewGuid(), paramTypesList[2], "true"));
//paramList.Add(new PermissionAssignmentParameter("Site", "Site permission parameter", Guid.NewGuid(), paramTypesList[0], sitesList[0].InternalId.ToString()));
//paramList.Add(new PermissionAssignmentParameter("Site", "Site permission parameter", Guid.NewGuid(), paramTypesList[0], sitesList[0].InternalId.ToString()));
//paramList.Add(new PermissionAssignmentParameter("Write", "Write permission parameter", Guid.NewGuid(), paramTypesList[2], "true"));
//paramList.Add(new PermissionAssignmentParameter("Create user", "Create user permission parameter", Guid.NewGuid(), paramTypesList[2], "true"));

//paramList.Add(new PermissionTemplateParameter("Site ZZZ", "Site ZZZ parameter", Guid.NewGuid(), paramTypesList[0], "*"));

////test permission templates
//ISet<PermissionTemplateParameter> ptpSet = new HashSet<PermissionTemplateParameter>();
//ptpSet.Add((PermissionTemplateParameter)paramList[3]);
//ptpSet.Add((PermissionTemplateParameter)paramList[18]);

//permTemplatesList.Add(new PermissionTemplate("Reader Site ZZZ", "Read", "Allows read", Guid.NewGuid(), ptpSet)); //should have both ZZZ params

//ptpSet.Clear();
//ptpSet.Add((PermissionTemplateParameter)paramList[4]);
//permTemplatesList.Add(new PermissionTemplate("Writer", "Write", "Allows write", Guid.NewGuid(), ptpSet));

//ptpSet.Clear();
//ptpSet.Add((PermissionTemplateParameter)paramList[5]);
//permTemplatesList.Add(new PermissionTemplate("Site", "Site", "A site permission", Guid.NewGuid(), ptpSet));

//ptpSet.Clear();
//ptpSet.Add((PermissionTemplateParameter)paramList[6]);
//permTemplatesList.Add(new PermissionTemplate("Create user", "Create", "Can create users", Guid.NewGuid(), ptpSet));

////test role templates
//ISet<RoleTemplateParameter> rtpSet = new HashSet<RoleTemplateParameter>();
//rtpSet.Add((RoleTemplateParameter)paramList[0]);
//rtpSet.Add((RoleTemplateParameter)paramList[1]);
//ISet<PermissionTemplate> ptSet = new HashSet<PermissionTemplate>();
//ptSet.Add(permTemplatesList[0]);
//ptSet.Add(permTemplatesList[2]);
//roleTemplatesList.Add(new RoleTemplate("Site Reader", "Can read from a a site", Guid.NewGuid(), ptSet, rtpSet));

//rtpSet.Clear();
//rtpSet.Add((RoleTemplateParameter)paramList[2]);
//ptSet.Clear();
//ptSet.Add(permTemplatesList[1]);
//roleTemplatesList.Add(new RoleTemplate("Writer", "Can write", Guid.NewGuid(), ptSet, rtpSet));

////test permission assignments
//ISet<PermissionAssignmentParameter> papSet = new HashSet<PermissionAssignmentParameter>();
//papSet.Add((PermissionAssignmentParameter)paramList[12]);
//permAssignmentsList.Add(new PermissionAssignment("Read permission", "Read", "Allows read", Guid.NewGuid(), permTemplatesList[0], papSet));

//papSet.Clear();
//papSet.Add((PermissionAssignmentParameter)paramList[13]);
//permAssignmentsList.Add(new PermissionAssignment("Read permission", "Read", "Allows read", Guid.NewGuid(), permTemplatesList[0], papSet));

//papSet.Clear();
//papSet.Add((PermissionAssignmentParameter)paramList[14]);
//permAssignmentsList.Add(new PermissionAssignment("Site permission", "Site", "A site permission", Guid.NewGuid(), permTemplatesList[2], papSet));

//papSet.Clear();
//papSet.Add((PermissionAssignmentParameter)paramList[15]);
//permAssignmentsList.Add(new PermissionAssignment("Site permission", "Site", "A site permission", Guid.NewGuid(), permTemplatesList[2], papSet));

//papSet.Clear();
//papSet.Add((PermissionAssignmentParameter)paramList[16]);
//permAssignmentsList.Add(new PermissionAssignment("Write permission", "Write", "Allows write", Guid.NewGuid(), permTemplatesList[1], papSet));

//papSet.Clear();
//papSet.Add((PermissionAssignmentParameter)paramList[17]);
//permAssignmentsList.Add(new PermissionAssignment("Create user permission", "Create", "Can create user", Guid.NewGuid(), permTemplatesList[3], papSet));

////test role assignments
//ISet<RoleAssignmentParameter> rapSet = new HashSet<RoleAssignmentParameter>();
//rapSet.Add((RoleAssignmentParameter)paramList[7]);
//rapSet.Add((RoleAssignmentParameter)paramList[10]);
//var paSet = new HashSet<PermissionAssignment>();
//paSet.Add(permAssignmentsList[0]);
//paSet.Add(permAssignmentsList[2]);
//roleAssignmentsList.Add(new RoleAssignment("Site reader", "Can read from a site", Guid.NewGuid(), roleTemplatesList[0], paSet, rapSet));

//rapSet.Clear();
//rapSet.Add((RoleAssignmentParameter)paramList[8]);
//rapSet.Add((RoleAssignmentParameter)paramList[11]);
//paSet.Clear();
//paSet.Add(permAssignmentsList[1]);
//paSet.Add(permAssignmentsList[3]);
//roleAssignmentsList.Add(new RoleAssignment("Site reader", "Can read from a site", Guid.NewGuid(), roleTemplatesList[0], paSet, rapSet));

//rapSet.Clear();
//rapSet.Add((RoleAssignmentParameter)paramList[9]);
//paSet.Clear();
//paSet.Add(permAssignmentsList[4]);
//roleAssignmentsList.Add(new RoleAssignment("Writer", "Can write", Guid.NewGuid(), roleTemplatesList[1], paSet, rapSet));

//PermissionAssignment permissionAssignment1 = PermissionAssignmentFactory.CreateNewPermissionAssignment(permTemplatesList[0]);
//RoleAssignment roleAssignment1 = RoleAssignmentFactory.CreateNewRoleAssignment(roleTemplatesList[0]);

////test users
//ISet<RoleAssignment> roleassSet = new HashSet<RoleAssignment>();
//ISet<PermissionAssignment> permassSet = new HashSet<PermissionAssignment>();
//roleassSet.Add(roleAssignmentsList[0]);
//roleassSet.Add(roleAssignmentsList[2]);
//permassSet.Add(permAssignmentsList[0]);
//permassSet.Add(permAssignmentsList[2]);
//permassSet.Add(permAssignmentsList[4]);
////            permassSet.ElementAt(0).RoleAssignment = roleassSet.ElementAt(0); //why did I do this...?

////test Salt etc.
//var password = new byte[256];
//password = System.Text.Encoding.UTF8.GetBytes("password123");
//var salt = CreateSalt(password.Length);
//var saltPw = PlaintextToSaltedHash(password, salt);
//string saltStr = Convert.ToBase64String(salt);
//string saltPwStr = Convert.ToBase64String(saltPw);

////temp
//permassSet.Clear();
//roleassSet.Clear();

//usersList.Add(new ApplicationUser("Luke", "Sandells", true, "luke.sandells@manufacturingintelligence.com.au", Guid.NewGuid(), orgsList[0], saltPwStr, saltStr, permassSet, roleassSet));

//password = System.Text.Encoding.UTF8.GetBytes("fuckthisshit");
//salt = CreateSalt(password.Length);
//saltPw = PlaintextToSaltedHash(password, salt);

//roleassSet.Clear();
//permassSet.Clear();
//roleassSet.Add(roleAssignmentsList[1]);
//permassSet.Add(permAssignmentsList[1]);
//permassSet.Add(permAssignmentsList[3]);
//permassSet.Add(permAssignmentsList[5]);

//usersList.Add(new ApplicationUser("Toby", "Russell", true, "toby.russell@manufacturingintelligence.com.au", Guid.NewGuid(), orgsList[0], Convert.ToBase64String(saltPw), Convert.ToBase64String(salt), permassSet, roleassSet));

////token test
//RefreshToken tok = new RefreshToken(new DateTime(1970, 1, 1), new DateTime(2018, 12, 31), Guid.NewGuid().ToString(), usersList[0]);