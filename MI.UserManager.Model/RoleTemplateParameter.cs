﻿using System;

namespace MI.UserManager.Model
{
    public class RoleTemplateParameter : TemplateParameter
    {
        public virtual RoleTemplate ParentRoleTemplate { get; set; }

        public RoleTemplateParameter()
        {
            DefaultValue = "";
        }

        public RoleTemplateParameter(string name, string description, Guid internalId, ParameterType type, string defaultValue) : base(name, description, internalId, type, defaultValue)
        {

        }
    }
}
