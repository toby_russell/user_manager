﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UserManager.Model.Factories
{
    public class PermissionAssignmentFactory
    {
        public static PermissionAssignment CreateNewPermissionAssignment(PermissionTemplate permissionTemplate)
        {
            var permissionParameters = new HashSet<PermissionAssignmentParameter>();

            foreach(var ptp in permissionTemplate.Parameters)
            {
                permissionParameters.Add(new PermissionAssignmentParameter(ptp.Name, ptp.Description, Guid.NewGuid(), ptp.Type, ptp.DefaultValue));
            }
           
            return new PermissionAssignment(permissionTemplate.Name, permissionTemplate.ClaimType, permissionTemplate.Description, Guid.NewGuid(), permissionTemplate, permissionParameters);
        }

        //public static PermissionAssignment CreateNewPermissionAssignment(PermissionTemplate permissionTemplate, RoleAssignment roleAssignment = null)
        //{
        //    var permissionParameters = new HashSet<PermissionAssignmentParameter>();

        //    foreach (var ptp in permissionTemplate.Parameters)
        //    {
        //        permissionParameters.Add(new PermissionAssignmentParameter(ptp.Name, ptp.Description, Guid.NewGuid(), ptp.Type, ptp.DefaultValue));
        //    }

        //    return new PermissionAssignment(permissionTemplate.Name, permissionTemplate.ClaimType, permissionTemplate.Description, Guid.NewGuid(), permissionTemplate, permissionParameters, roleAssignment);
        //}
    }
}
