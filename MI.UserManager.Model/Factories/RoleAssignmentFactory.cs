﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MI.UserManager.Model.Factories
{
    public class RoleAssignmentFactory
    {
        public static RoleAssignment CreateNewRoleAssignment(RoleTemplate roleTemplate)
        {
            var parameters = new HashSet<RoleAssignmentParameter>();
            var permissions = new HashSet<PermissionAssignment>();

            foreach(var rtp in roleTemplate.Parameters)
            {
                parameters.Add(new RoleAssignmentParameter(rtp.Name, rtp.Description, Guid.NewGuid(), rtp.Type, rtp.DefaultValue));
            }

            foreach(var permissionTemplate in roleTemplate.PermissionTemplates)
            {
                permissions.Add(PermissionAssignmentFactory.CreateNewPermissionAssignment(permissionTemplate));
            }

            return new RoleAssignment(roleTemplate.Name, roleTemplate.Description, Guid.NewGuid(), roleTemplate, permissions, parameters);
        }
    }
}
