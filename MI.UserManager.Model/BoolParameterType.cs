﻿using System;

namespace MI.UserManager.Model
{
    public class BoolParameterType : ParameterType<bool>
    {
        public BoolParameterType()
        {
            
        }

        public BoolParameterType(string name, string description, Guid internalId) : base(name, description, internalId)
        {
        }

        public override string SerialiseParameter<T>(Parameter parameter, T parameterValue)
        {
            if(parameterValue is bool)
                return parameterValue.ToString();
            throw new IncorrectParameterTypeValueException($"{ parameter.Name } with parameterId { parameter.ParameterId } has been provided with a value not a bool.");
        }

        public override T DeserialiseParameter<T>(Parameter parameter)
        {
            if(typeof(T) != typeof(bool))
                throw new IncorrectParameterTypeValueException($"{parameter.Name} with parameterId {parameter.ParameterId} is a bool, but an attmept has been made to desrialise it to a different type.");

            var param = parameter is AssignmentParameter assignmentParameter
                ? assignmentParameter.Value
                : ((TemplateParameter)parameter).DefaultValue;
            

            var result = bool.TryParse(param, out var deserialised);
            if (deserialised)
                return (T) Convert.ChangeType(result, typeof(bool));
            else
                throw new IncorrectParameterTypeValueException(
                    $"{parameter.Name} with parameterId {parameter.ParameterId} has been provided with a value not a bool.");
        }
    }
}