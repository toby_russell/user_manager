﻿using System;

namespace MI.UserManager.Model
{
    public abstract class ParameterType
    {
        public virtual int ParameterTypeId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string TypeDiscriminator 
            => GetType().ToString();

        public abstract string SerialiseParameter<T>(Parameter parameter, T parameterValue);
        public abstract T DeserialiseParameter<T>(Parameter parameter);

        public ParameterType()
        {

        }

        public ParameterType(string name, string description, Guid internalId)
        {
            Name = name;
            Description = description;
            InternalId = internalId;
        }
    }

    public abstract class ParameterType<T> : ParameterType
    {
        public ParameterType()
        {

        }

        public ParameterType(string name, string description, Guid internalId)
            : base(name, description, internalId)
        {
        }
    }
}