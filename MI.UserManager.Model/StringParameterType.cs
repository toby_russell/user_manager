﻿using System;

namespace MI.UserManager.Model
{
    public class StringParameterType : ParameterType<string>
    {
        public StringParameterType()
        {
        }

        public StringParameterType(string name, string description, Guid internalId) : base(name, description, internalId)
        {
        }

        public override string SerialiseParameter<T>(Parameter parameter, T parameterValue)
        {
            if (parameterValue is string)
                return parameterValue.ToString();
            throw new IncorrectParameterTypeValueException($"{ parameter.Name } with parameterId { parameter.ParameterId } has been provided with a value not a string.");
        }

        public override T DeserialiseParameter<T>(Parameter parameter)
        {
            if (typeof(T) != typeof(string))
                throw new IncorrectParameterTypeValueException($"{parameter.Name} with parameterId {parameter.ParameterId} is a string, but an attmept has been made to deserialise it to a different type.");

            var param = parameter is AssignmentParameter assignmentParameter
                ? assignmentParameter.Value
                : ((TemplateParameter)parameter).DefaultValue;


            switch (parameter)
            {
                case AssignmentParameter _ when param != null:
                    return (T)Convert.ChangeType(param, typeof(string));
                case TemplateParameter _:
                    return (T) Convert.ChangeType(param ?? "", typeof(string));
                default:
                    throw new IncorrectParameterTypeValueException(
                        $"{parameter.Name} with parameterId {parameter.ParameterId} has been provided with a value not a string.");
            }
        }
    }
}