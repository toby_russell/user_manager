﻿using System;

namespace MI.UserManager.Model
{
    public class PermissionTemplateParameter : TemplateParameter
    {
        public virtual PermissionTemplate ParentPermissionTemplate { get; set; }

        public PermissionTemplateParameter()
        {
            DefaultValue = "";
        }

        public PermissionTemplateParameter(string name, string description, Guid internalId, ParameterType type, string defaultValue) : base(name, description, internalId, type, defaultValue)
        {

        }
    }
}
