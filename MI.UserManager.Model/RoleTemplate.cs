﻿using System;
using System.Collections.Generic;

//using System.ComponentModel.DataAnnotations.Schema;

namespace MI.UserManager.Model
{
    public class RoleTemplate
    {
        public virtual int RoleTemplateId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }

        //either a list here, or a single object in the PermissionTemplate
        public virtual ISet<PermissionTemplate> PermissionTemplates { get; set; } = new HashSet<PermissionTemplate>();
        public virtual ISet<RoleTemplateParameter> Parameters { get; set; } = new HashSet<RoleTemplateParameter>();

        public RoleTemplate()
        {

        }

        public RoleTemplate(string roleTemplateName, string description, Guid internalId)
        {
            Name = roleTemplateName;
            Description = description;
            InternalId = internalId;
        }

        public RoleTemplate(string roleTemplateName, string description, Guid internalId, ISet<PermissionTemplate> permissionTemplates)
            : this(roleTemplateName, description, internalId)
        {
            PermissionTemplates.UnionWith(permissionTemplates);

            foreach(var permission in permissionTemplates)
            {
                foreach (var parameter in permission.Parameters)
                {
                    Parameters.Add(new RoleTemplateParameter(parameter.Name, parameter.Description, Guid.NewGuid(), parameter.Type, parameter.DefaultValue));
                }
            }
        }

        public RoleTemplate(string roleTemplateName, string description, Guid internalId, ISet<PermissionTemplate> permissionTemplates, ISet<RoleTemplateParameter> parameterTemplates)
            : this(roleTemplateName, description, internalId)
        {
            //foreach (RoleTemplateParameter rtp in parameterTemplates)
            //{
            //    rtp.ParentRoleTemplate = this;
            //}

            PermissionTemplates.UnionWith(permissionTemplates);
            Parameters.UnionWith(parameterTemplates);
        }
    }
}
