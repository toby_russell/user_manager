﻿using System;

namespace MI.UserManager.Model
{
    public class RefreshToken
    {
        public virtual int Id { get; set; }
        public virtual DateTime IssuedUtc { get; set; }
        public virtual DateTime ExpiresUtc { get; set; }
        public virtual string Token { get; set; }
        
        public virtual ApplicationUser User { get; set; }

        public RefreshToken()
        {

        }

        public RefreshToken(DateTime issuedUtc, DateTime expiresUtc, string token, ApplicationUser user)
        {
            IssuedUtc = issuedUtc;
            ExpiresUtc = expiresUtc;
            Token = token;
            User = user;
        }
    }
}
