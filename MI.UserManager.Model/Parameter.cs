﻿using System;

namespace MI.UserManager.Model
{
    public abstract class Parameter
    {
        public virtual int ParameterId { get; set; }
        public virtual Guid InternalId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Discriminator => GetType().ToString();

        public virtual ParameterType Type { get; set; }

        public Parameter()
        {

        }

        public Parameter(string name, string description, Guid internalId, ParameterType type)
        {
            Name = name;
            Description = description;
            InternalId = internalId;
            Type = type;
        }
    }


    public abstract class AssignmentParameter : Parameter
    {
        public virtual string Value { get; set; }

        protected AssignmentParameter()
        {
        }

        protected AssignmentParameter(string name, string description, Guid internalId, ParameterType type, string value) : base(name, description, internalId, type)
        {
            Value = value;
        }

    }

    public abstract class TemplateParameter : Parameter
    {
        public virtual string DefaultValue { get; set; }

        protected TemplateParameter()
        {
        }

        protected TemplateParameter(string name, string description, Guid internalId, ParameterType type, string defaultValue) : base(name, description, internalId, type)
        {
            DefaultValue = defaultValue;
        }
    }
}
