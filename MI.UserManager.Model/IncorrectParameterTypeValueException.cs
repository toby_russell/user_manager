﻿using System;

namespace MI.UserManager.Model
{
    public class IncorrectParameterTypeValueException : Exception
    {
        public IncorrectParameterTypeValueException(string message) : base(message) { }
    }
}