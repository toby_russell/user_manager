﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MI.UserManager.Model
{
    public class SiteParameterType : ParameterType<Site>
    {
        public SiteParameterType()
        {
        }

        public SiteParameterType(string name, string description, Guid internalId)
            : base(name, description, internalId)
        {

        }


        public override string SerialiseParameter<T>(Parameter parameter, T parameterValue)
        {
            if (parameterValue is Site)
                return parameterValue.ToString();
            else if (parameterValue is ICollection<Site>)
            {
                var counter = 0;
                var sb = new StringBuilder();
                var list = ((ICollection<Site>) parameterValue).ToList();
                list.ForEach(s => {
                    sb.Append(s.InternalId);
                    if (list.Count > 0 && counter < list.Count)
                        sb.Append(", ");
                    counter++;
                });
                return sb.ToString();
            }
            else
                throw new IncorrectParameterTypeValueException($"{ parameter.Name } of type Site with parameterId { parameter.ParameterId } has been provided with a value not a Site.");
        }

        public override T DeserialiseParameter<T>(Parameter parameter)
        {
            if (typeof(T).BaseType != typeof(ICollection<string>))
                throw new IncorrectParameterTypeValueException($"{parameter.Name} with parameterId {parameter.ParameterId} is a site ID string, but an attmept has been made to deserialise it to a different type.");

            var param = parameter is AssignmentParameter assignmentParameter
                ? assignmentParameter.Value
                : ((TemplateParameter)parameter).DefaultValue;

            var val = new [] {""};
            switch (parameter)
            {
                case AssignmentParameter _ when param != null:
                    return (T) Convert.ChangeType(param.Split(','), typeof(string));
                case TemplateParameter _:
                    return (T)Convert.ChangeType(param?.Split(',') ?? val, typeof(string));
                default:
                    throw new IncorrectParameterTypeValueException(
                        $"{parameter.Name} with parameterId {parameter.ParameterId} has been provided with a value not a string.");
            }
        }
    }
}