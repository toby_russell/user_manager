﻿using System;
using System.Collections.Generic;

namespace MI.UserManager.Model
{
    public class RoleAssignment
    {
        public virtual int RoleAssignmentId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual Guid InternalId { get; set; }
        //this is provided for reporting rather than operational use  
        public virtual RoleTemplate RoleTemplate { get; set; }

        //this relationship has been moved to a nullable field in the PermissionAssignment
        public virtual ISet<PermissionAssignment> PermissionAssignments { get; set; } = new HashSet<PermissionAssignment>();
        public virtual ISet<RoleAssignmentParameter> Parameters { get; set; } = new HashSet<RoleAssignmentParameter>();

        public RoleAssignment()
        {

        }

        public RoleAssignment(string name, string description, Guid internalId, RoleTemplate roleTemplate)
        {
            Name = name;
            Description = description;
            InternalId = internalId;
            RoleTemplate = roleTemplate;
        }

        public RoleAssignment(string name, string description, Guid internalId, RoleTemplate roleTemplate, ISet<PermissionAssignment> permissionAssignments, ISet<RoleAssignmentParameter> parameters)
            : this(name, description, internalId, roleTemplate)
        {
            //foreach(var rap in parameters)
            //{
            //    rap.ParentRoleAssignment = this;
            //}

            PermissionAssignments.UnionWith(permissionAssignments);
            Parameters.UnionWith(parameters);
        }
    }
}
